#include <iostream>
#include <vector>
#include <algorithm>
#include <fstream>
#include<time.h>
#include <iomanip>

using namespace std;

typedef struct T_Thing
{
    T_Thing(uint32_t weight, uint32_t price, bool in_bag, uint32_t counter):
            m_weight(weight),
            m_price(price),
            m_in_bag_flag(in_bag),
            m_counter(counter)
    {
        m_score = m_price / (m_weight * 1.0);
    };

    uint32_t m_weight;
    uint32_t m_price;
    double m_score;
    bool m_in_bag_flag;
    uint32_t m_counter;
} T_Thing;

vector<T_Thing> G_data;
uint32_t G_max_weight;
uint32_t G_max_price;

vector<T_Thing> G_solution;

uint32_t G_data_size;
bool read_instance(int& id, ifstream& in)
{
    in >> id;
    in >> G_data_size;
    in >> G_max_weight;

    char check_new_line;
    uint32_t temp_weight, temp_price, counter = 0;
    while(in.get(check_new_line) && check_new_line != '\n')
    {

        in >> temp_weight;
        in >> temp_price;
        G_data.push_back(T_Thing(temp_weight, temp_price, false, counter++));
    }

    if(!in.good())
    {
        in.ignore();
        return false;
    }

    return true;
}

bool read_solved_data(int id, ifstream & in, uint32_t& optimal_price)
{
    int data_size;
    int temp_id;
    in >> temp_id;
    in >> data_size;
    in >> optimal_price;
    if(temp_id != id)
    {
        cerr << "error in parsing solved data" << endl;
    }
    char check_new_line;
    while(in.get(check_new_line) && check_new_line != '\n');

    if(!in.good())
    {
        in.ignore();
        return false;
    }

    return true;

}

void solve_bruteforce(const uint32_t actual_depth, const uint32_t weight, const uint32_t price)
{
    if(price > G_max_price && weight + G_data[actual_depth].m_weight <= G_max_weight)
    {
        G_solution = G_data;
        G_max_price = price;
    }

    if(actual_depth < G_data.size())
    {
        //put inside
        G_data[actual_depth].m_in_bag_flag = true;
        solve_bruteforce(actual_depth + 1, weight + G_data[actual_depth].m_weight,
                         price + G_data[actual_depth].m_price);

        G_data[actual_depth].m_in_bag_flag = false;
        solve_bruteforce(actual_depth+1, weight , price );

    }
}


//bool cmp (T_Thing i,T_Thing j) { return (i.m_score > j.m_score); } //pomer

//bool cmp (T_Thing i,T_Thing j) { return (i.m_weight < j.m_weight); } //vaha
bool cmp (T_Thing i,T_Thing j) { return (i.m_price > j.m_price); } // cena


void fast_solve(void)
{
    G_solution = G_data;
    sort (G_data.begin(), G_data.end(), cmp);

    uint32_t acc_weight = 0;

    for(auto it: G_data)
    {
        if(acc_weight + it.m_weight > G_max_weight)
            continue;
        else
        {
            acc_weight += it.m_weight;
            G_max_price+= it.m_price;
            G_solution[it.m_counter].m_in_bag_flag = true;

        }
    }

}

int main(int argc, char * argv[])
{
    int id;
    vector<T_Thing>data;
    vector<double> failures;
    vector<double> brute_times;
    vector<double> heur_times;
    clock_t begin;
    clock_t end;
    ifstream input;
    ifstream check;
    ofstream of;

    if(argc < 3)
    {
        cout << "invalid arguments" << endl;
        return 1;
    }
    input.open(argv[1]);
    check.open(argv[2]);
    of.open("out.txt");

    while(read_instance(id, input))
    {
        uint32_t optimal_price;

        read_solved_data(id, check, optimal_price);

        begin = clock();
        solve_bruteforce(0,0,0);
        end = clock();
        of << id << " " << G_data.size() <<" " << G_max_price;
        G_max_price = 0;

        brute_times.push_back( (end - begin)/((CLOCKS_PER_SEC/1000)*1.0));
        begin = clock();
        fast_solve();
        end = clock();
        heur_times.push_back( (end - begin)/((CLOCKS_PER_SEC/1000)*1.0));

        failures.push_back(abs(optimal_price - G_max_price)/ (optimal_price));


        for(auto it : G_solution)
            of << " " << it.m_in_bag_flag;

        of << " clock: " << end - begin << endl;

        G_max_weight = 0;
        G_max_price = 0;
        G_data = vector<T_Thing>();

    }
    double sum = 0;
    double max = 0;
    for(auto it:failures)
    {
        sum += it;
        if(it > max)
            max = it;
    }

    double brute_avg_sum = 0;
    for(auto it:brute_times)
        brute_avg_sum += it;

    double heur_time_avg_sum = 0;
    for(auto it:heur_times)
        heur_time_avg_sum+=it;

    cout << "data size: " << G_data_size << endl;
    cout << "brute avg time [ms]: " << brute_avg_sum/(brute_times.size()/1.0) << endl;
    cout << "heur avg time [ms]: " <<  heur_time_avg_sum/(heur_times.size()/1.0) << endl;
    cout << "avg failure: " << (sum/(failures.size() * 1.0)) << endl;
    cout << "max failure: " << max << endl;
    cout << "------------------------------" << endl;

    return 0;
}