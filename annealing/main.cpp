#include <iostream>
#include <vector>
#include <bitset>
#include <fstream>
#include <random>
#include <chrono>
#include <algorithm>


#define BIT_SET_LEN 500
using namespace std;



typedef struct T_Thing
{
    T_Thing(uint32_t weight, uint32_t price, bool in_bag, uint32_t counter):
            m_weight(weight),
            m_price(price),
            m_in_bag_flag(in_bag),
            m_counter(counter)
    {
        m_score = m_price / m_weight;
    };

    uint32_t m_weight;
    uint32_t m_price;
    uint32_t m_score;
    bool m_in_bag_flag;
    uint32_t m_counter;
} T_Thing;


typedef struct T_State
{
    T_State(uint32_t weight, uint32_t price):m_weight(weight), m_price(price)
    {};
    T_State():m_weight(0), m_price(0){};
    bitset<BIT_SET_LEN> value;
    uint32_t m_weight;
    uint32_t m_price;
} T_State;


bool read_instance(int& id, ifstream& in, uint32_t & data_size, uint32_t& max_weight, vector<T_Thing>& data)
{
    in >> id;
    in >> data_size;
    in >> max_weight;
    data = vector<T_Thing>();

    char check_new_line;
    uint32_t temp_weight, temp_price, counter = 0;
    while(in.get(check_new_line) && check_new_line != '\n' && counter <= (data_size - 1))
    {

        in >> temp_weight;
        in >> temp_price;
        data.push_back(T_Thing(temp_weight, temp_price, false, counter++));
    }

    if(!in.good())
    {
        in.ignore();
        return false;
    }

    return true;
}


bool read_solved_data(int id, ifstream & in, uint32_t& optimal_price)
{
    int data_size;
    int temp_id;
    in >> temp_id;
    in >> data_size;
    in >> optimal_price;
    if(temp_id != id)
    {
        cerr << "error in parsing solved data" << endl;
    }
    char check_new_line;
    while(in.get(check_new_line) && check_new_line != '\n');

    if(!in.good())
    {
        in.ignore();
        return false;
    }

    return true;

}

double sum(vector<T_Thing>& data)
{
   double sum = 0;
    for(auto it : data)
        sum += it.m_price;

    return sum;
}

double max_price(vector<T_Thing>& data)
{
    double max = 0;
    for (auto it : data)
        if(it.m_price > max)
            max = it.m_price;

    return max;
}

double init_Temp(vector<T_Thing>& data, double max_price_percent)
{
    double price_max = max_price(data);
   // double temp = ( sum_price * 0.1 )/(-log(0.5));

    double temp =  max_price_percent * price_max * 1.4427; // t = max_price/-log(0.5) 15% chyba z maximalni ceny se prijme
                                                // s pravdepodobnosti 0.5
    return temp;
}

bool frozen(double temp, double max_price, double frozen_max_price_percent)
{
    if(temp < (0.250 * max_price * frozen_max_price_percent)) // 10% chyba s pravdepodobnosti 1/E**4
    {
        return true;
    }

    return false;
}

T_State find_neighbour(T_State state,
                       vector<T_Thing>& data,
                       uint32_t max_weight,
                       default_random_engine& generator,
                       uniform_int_distribution<int>& dist, double better_rate)
{
    T_State new_state;

    double err = (dist(generator)/(double)(int)dist.max());

    if(err < better_rate) {

        //improve
        new_state = state;
        for (auto it = data.begin(); it != data.end(); it++) {
            if (new_state.m_weight + it->m_weight <= max_weight && !new_state.value[it - data.begin()]) {
                new_state.value.flip(it - data.begin());
                new_state.m_price += it->m_price;
                new_state.m_weight += it->m_weight;
                break;
            }
        }
    }
    else {
        do {
            new_state = state;
            uint32_t index_to_flip = (uint32_t) ((uint32_t) dist(generator) % data.size());
            T_Thing item = data[index_to_flip];
            if (new_state.value[index_to_flip]) {
                new_state.m_price -= item.m_price;
                new_state.m_weight -= item.m_weight;

            }
            else {
                new_state.m_price += item.m_price;
                new_state.m_weight += item.m_weight;
            }
            new_state.value.flip(index_to_flip);

        }
        while (new_state.m_weight > max_weight);
    }
    return new_state;
}


T_State get_new_state(T_State& state,
                      default_random_engine& generator,
                      uniform_int_distribution<int>& dist,
                      double temperature,
                      vector<T_Thing>& data,
                      uint32_t max_weight,
                      double better_rate
)
{
    T_State new_state = find_neighbour(state, data, max_weight, generator, dist, better_rate);

    int32_t delta = new_state.m_price - state.m_price;
    if(delta > 0)
        return new_state;

    double err = (dist(generator)/(double)(int)dist.max());

    if(err < exp(delta/temperature))
        return new_state;

    return state;
}




T_State init_state( vector<T_Thing>& data, uint32_t max_weight, default_random_engine& generator,
                   uniform_int_distribution<int>& dist)
{

    T_State state;
    for(uint32_t i = 0; i< data.size() * 2; i++)
        state = find_neighbour(state, data, max_weight, generator, dist, 0);

    return state;
}
//#define DEBUG

uint32_t G_cnt = 0;
T_State annealing(time_t& begin, uint32_t max_weight, vector<T_Thing>&data, double max_price_percent, double cool_rate,
                  double better_rate, double frozen_max_price_percent, double temp_divider)
{
    double temp = init_Temp(data, max_price_percent);
    time_t end = clock();
    std::default_random_engine generator(begin - end);
    std::uniform_int_distribution<int> distribution(0,(uint32_t)0x0FFFFFFF);
    T_State state = init_state( data, max_weight, generator, distribution);
    T_State max_state = state;
    uint32_t no_new_state_cnt = 0;
    double price_max = max_price(data);

    do
    {
<<<<<<< HEAD

        for(uint32_t i = 0; i < data.size() * ceil((temp)/temp_divider) ; i++)
=======
        state = max_state;
        for(uint32_t i = 0; i < data.size() * ceil((temp)) ; i++)
>>>>>>> SAT
        {

            T_State new_state = get_new_state(state, generator, distribution, temp, data, max_weight, better_rate);

            if(new_state.value != state.value)
            {
                state = new_state;
                no_new_state_cnt = 0;
            } else
            {
                no_new_state_cnt++;
            }
#ifdef DEBUG
            G_cnt++;
            cout << state.m_price << endl;
#endif
            if(state.m_price > max_state.m_price)
                max_state = state;


        }

        temp = temp * cool_rate;

    }while( no_new_state_cnt < data.size()/2 && !frozen(temp, price_max, frozen_max_price_percent));

    return max_state;
}



double avg(vector<double>&nums)
{
    double sum = 0;
    for (auto it : nums)
        sum+= it;

    return sum/nums.size();
}

double max(vector<double>&nums)
{
    double max = 0;
    for(auto it : nums)
        if(max < it) {
            max = it;
        }
    return max;
}

bool cmp(double a, double b){return (a > b);}

double median(vector<double> & nums)
{
    sort(nums.begin(), nums.end(), cmp );

    return nums[nums.size()/2];
}

bool cmp_score (T_Thing i,T_Thing j) { return (i.m_score > j.m_score); }

int main(int argc, char ** argv)
{
    if (argc < 3)
    {
        cout << "invalid arguments" << endl;
        return 1;
    }
    int id;
    cerr << "#citlivost na frozen ratio" << endl;
    cerr << "#better rate = 0.1, coolrate = 0.8; init_temp_ratio = 0.15; frozen rate = 0.1" << endl;
    cerr <<"# equilibrium divider ;avg FAIL; avg TIME; max fail; max_time" << endl;
   // for(double i = 1; i < 100; i += 1) {
        ifstream input;
        ifstream check;
        ofstream of;
        of.open("out.txt");
        time_t begin = clock();
        uint32_t data_size;

        vector<double> avg_failures;
        vector<double> avg_failures2;
        vector<double> time;

        input.open(argv[1]);
        check.open(argv[2]);
        vector<double> failures;
        vector<double> failures2;

        vector<T_Thing> data;
        uint32_t max_weight;
#ifdef DEBUG
        read_instance(id, input, data_size, max_weight, data);
#else
        while (read_instance(id, input, data_size, max_weight, data)) {
#endif
            sort(data.begin(), data.end(), cmp_score);
            uint32_t optimal_price = 1;
            read_solved_data(id, check, optimal_price);
            T_State result;

            time_t b, e;

            b = clock();
            result = annealing(begin, max_weight, data, 0.20, 0.75, 0.2p, 0.1, 7);
            e = clock();


            time.push_back((e - b) / ((CLOCKS_PER_SEC / 1000) * 1.0));

            failures.push_back((abs((int) result.m_price - (int) optimal_price) / (double) (optimal_price)));
            //cerr << id << " " << failures.back() << " " << time.back() << endl;
#ifndef DEBUG
        }
#endif
        input.close();
        check.close();
    cerr << 500 <<";" << avg(failures)<<";" << avg(time)<<";" << max(failures)<<";" << max(time) <<";" << endl;
   // }

    return 0;
}
