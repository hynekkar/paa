	.file	"main.cpp"
	.section	.rodata
	.type	_ZStL19piecewise_construct, @object
	.size	_ZStL19piecewise_construct, 1
_ZStL19piecewise_construct:
	.zero	1
	.section	.text._ZnwmPv,"axG",@progbits,_ZnwmPv,comdat
	.weak	_ZnwmPv
	.type	_ZnwmPv, @function
_ZnwmPv:
.LFB429:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	-16(%rbp), %rax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE429:
	.size	_ZnwmPv, .-_ZnwmPv
	.section	.text._ZStorSt13_Ios_OpenmodeS_,"axG",@progbits,_ZStorSt13_Ios_OpenmodeS_,comdat
	.weak	_ZStorSt13_Ios_OpenmodeS_
	.type	_ZStorSt13_Ios_OpenmodeS_, @function
_ZStorSt13_Ios_OpenmodeS_:
.LFB985:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movl	%edi, -4(%rbp)
	movl	%esi, -8(%rbp)
	movl	-4(%rbp), %eax
	orl	-8(%rbp), %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE985:
	.size	_ZStorSt13_Ios_OpenmodeS_, .-_ZStorSt13_Ios_OpenmodeS_
	.local	_ZStL8__ioinit
	.comm	_ZStL8__ioinit,1,1
	.section	.text._ZN7T_ThingC2Ejjbj,"axG",@progbits,_ZN7T_ThingC5Ejjbj,comdat
	.align 2
	.weak	_ZN7T_ThingC2Ejjbj
	.type	_ZN7T_ThingC2Ejjbj, @function
_ZN7T_ThingC2Ejjbj:
.LFB1990:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	%rdi, -8(%rbp)
	movl	%esi, -12(%rbp)
	movl	%edx, -16(%rbp)
	movl	%ecx, %eax
	movl	%r8d, -24(%rbp)
	movb	%al, -20(%rbp)
	movq	-8(%rbp), %rax
	movl	-12(%rbp), %edx
	movl	%edx, (%rax)
	movq	-8(%rbp), %rax
	movl	-16(%rbp), %edx
	movl	%edx, 4(%rax)
	movq	-8(%rbp), %rax
	movzbl	-20(%rbp), %edx
	movb	%dl, 12(%rax)
	movq	-8(%rbp), %rax
	movl	-24(%rbp), %edx
	movl	%edx, 16(%rax)
	movq	-8(%rbp), %rax
	movl	4(%rax), %eax
	movq	-8(%rbp), %rdx
	movl	(%rdx), %ecx
	movl	$0, %edx
	divl	%ecx
	movl	%eax, %edx
	movq	-8(%rbp), %rax
	movl	%edx, 8(%rax)
	nop
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1990:
	.size	_ZN7T_ThingC2Ejjbj, .-_ZN7T_ThingC2Ejjbj
	.weak	_ZN7T_ThingC1Ejjbj
	.set	_ZN7T_ThingC1Ejjbj,_ZN7T_ThingC2Ejjbj
	.globl	G_max_weight
	.bss
	.align 4
	.type	G_max_weight, @object
	.size	G_max_weight, 4
G_max_weight:
	.zero	4
	.globl	G_max_price
	.align 4
	.type	G_max_price, @object
	.size	G_max_price, 4
G_max_price:
	.zero	4
	.globl	G_data_size
	.align 4
	.type	G_data_size, @object
	.size	G_data_size, 4
G_data_size:
	.zero	4
	.globl	G_data
	.align 16
	.type	G_data, @object
	.size	G_data, 24
G_data:
	.zero	24
	.text
	.globl	_Z13read_instanceRiRSt14basic_ifstreamIcSt11char_traitsIcEE
	.type	_Z13read_instanceRiRSt14basic_ifstreamIcSt11char_traitsIcEE, @function
_Z13read_instanceRiRSt14basic_ifstreamIcSt11char_traitsIcEE:
.LFB1992:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$64, %rsp
	movq	%rdi, -56(%rbp)
	movq	%rsi, -64(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movq	-64(%rbp), %rax
	movq	-56(%rbp), %rdx
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	_ZNSirsERi
	movq	-64(%rbp), %rax
	movl	$G_data_size, %esi
	movq	%rax, %rdi
	call	_ZNSirsERj
	movq	-64(%rbp), %rax
	movl	$G_max_weight, %esi
	movq	%rax, %rdi
	call	_ZNSirsERj
	movl	$0, -36(%rbp)
.L10:
	movq	-64(%rbp), %rax
	leaq	-45(%rbp), %rdx
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	_ZNSi3getERc
	movq	(%rax), %rdx
	subq	$24, %rdx
	movq	(%rdx), %rdx
	addq	%rdx, %rax
	movq	%rax, %rdi
	call	_ZNKSt9basic_iosIcSt11char_traitsIcEEcvbEv
	testb	%al, %al
	je	.L7
	movzbl	-45(%rbp), %eax
	cmpb	$10, %al
	je	.L7
	movl	$1, %eax
	jmp	.L8
.L7:
	movl	$0, %eax
.L8:
	testb	%al, %al
	je	.L9
	movq	-64(%rbp), %rax
	leaq	-44(%rbp), %rdx
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	_ZNSirsERj
	movq	-64(%rbp), %rax
	leaq	-40(%rbp), %rdx
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	_ZNSirsERj
	movl	-36(%rbp), %eax
	leal	1(%rax), %edx
	movl	%edx, -36(%rbp)
	movl	-40(%rbp), %edx
	movl	-44(%rbp), %esi
	leaq	-32(%rbp), %rdi
	movl	%eax, %r8d
	movl	$0, %ecx
	call	_ZN7T_ThingC1Ejjbj
	leaq	-32(%rbp), %rax
	movq	%rax, %rsi
	movl	$G_data, %edi
	call	_ZNSt6vectorI7T_ThingSaIS0_EE9push_backEOS0_
	jmp	.L10
.L9:
	movq	-64(%rbp), %rax
	movq	(%rax), %rax
	subq	$24, %rax
	movq	(%rax), %rax
	movq	%rax, %rdx
	movq	-64(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, %rdi
	call	_ZNKSt9basic_iosIcSt11char_traitsIcEE4goodEv
	xorl	$1, %eax
	testb	%al, %al
	je	.L11
	movq	-64(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSi6ignoreEv
	movl	$0, %eax
	jmp	.L13
.L11:
	movl	$1, %eax
.L13:
	movq	-8(%rbp), %rcx
	xorq	%fs:40, %rcx
	je	.L14
	call	__stack_chk_fail
.L14:
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1992:
	.size	_Z13read_instanceRiRSt14basic_ifstreamIcSt11char_traitsIcEE, .-_Z13read_instanceRiRSt14basic_ifstreamIcSt11char_traitsIcEE
	.section	.rodata
.LC0:
	.string	"invalid arguments"
.LC1:
	.string	"out.txt"
	.text
	.globl	main
	.type	main, @function
main:
.LFB1993:
	.cfi_startproc
	.cfi_personality 0x3,__gxx_personality_v0
	.cfi_lsda 0x3,.LLSDA1993
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$1608, %rsp
	.cfi_offset 3, -24
	movl	%edi, -1604(%rbp)
	movq	%rsi, -1616(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movl	-1596(%rbp), %edx
	movl	-1592(%rbp), %eax
	addl	%edx, %eax
	movl	%eax, -1588(%rbp)
	movl	-1588(%rbp), %eax
	addl	%eax, %eax
	movl	%eax, -1588(%rbp)
	movl	-1604(%rbp), %eax
	cmpl	-1588(%rbp), %eax
	jnb	.L16
	movl	$.LC0, %esi
	movl	$_ZSt4cout, %edi
.LEHB0:
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
	movl	$_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_, %esi
	movq	%rax, %rdi
	call	_ZNSolsEPFRSoS_E
	movl	$1, %ebx
	jmp	.L20
.L16:
	leaq	-1072(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSt14basic_ifstreamIcSt11char_traitsIcEEC1Ev
.LEHE0:
	leaq	-544(%rbp), %rax
	movq	%rax, %rdi
.LEHB1:
	call	_ZNSt14basic_ifstreamIcSt11char_traitsIcEEC1Ev
.LEHE1:
	leaq	-1584(%rbp), %rax
	movq	%rax, %rdi
.LEHB2:
	call	_ZNSt14basic_ofstreamIcSt11char_traitsIcEEC1Ev
.LEHE2:
	movq	-1616(%rbp), %rax
	addq	$8, %rax
	movq	(%rax), %rcx
	leaq	-1072(%rbp), %rax
	movl	$8, %edx
	movq	%rcx, %rsi
	movq	%rax, %rdi
.LEHB3:
	call	_ZNSt14basic_ifstreamIcSt11char_traitsIcEE4openEPKcSt13_Ios_Openmode
	movq	-1616(%rbp), %rax
	addq	$16, %rax
	movq	(%rax), %rcx
	leaq	-544(%rbp), %rax
	movl	$8, %edx
	movq	%rcx, %rsi
	movq	%rax, %rdi
	call	_ZNSt14basic_ifstreamIcSt11char_traitsIcEE4openEPKcSt13_Ios_Openmode
	movl	$32, %esi
	movl	$16, %edi
	call	_ZStorSt13_Ios_OpenmodeS_
	movl	%eax, %edx
	leaq	-1584(%rbp), %rax
	movl	$.LC1, %esi
	movq	%rax, %rdi
	call	_ZNSt14basic_ofstreamIcSt11char_traitsIcEE4openEPKcSt13_Ios_Openmode
.L19:
	leaq	-1072(%rbp), %rdx
	leaq	-1600(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	_Z13read_instanceRiRSt14basic_ifstreamIcSt11char_traitsIcEE
.LEHE3:
	testb	%al, %al
	je	.L18
	jmp	.L19
.L18:
	movl	$0, %ebx
	leaq	-1584(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSt14basic_ofstreamIcSt11char_traitsIcEED1Ev
	leaq	-544(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSt14basic_ifstreamIcSt11char_traitsIcEED1Ev
	leaq	-1072(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSt14basic_ifstreamIcSt11char_traitsIcEED1Ev
.L20:
	movl	%ebx, %eax
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	je	.L24
	jmp	.L28
.L27:
	movq	%rax, %rbx
	leaq	-1584(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSt14basic_ofstreamIcSt11char_traitsIcEED1Ev
	jmp	.L22
.L26:
	movq	%rax, %rbx
.L22:
	leaq	-544(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSt14basic_ifstreamIcSt11char_traitsIcEED1Ev
	jmp	.L23
.L25:
	movq	%rax, %rbx
.L23:
	leaq	-1072(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSt14basic_ifstreamIcSt11char_traitsIcEED1Ev
	movq	%rbx, %rax
	movq	%rax, %rdi
.LEHB4:
	call	_Unwind_Resume
.LEHE4:
.L28:
	call	__stack_chk_fail
.L24:
	addq	$1608, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1993:
	.globl	__gxx_personality_v0
	.section	.gcc_except_table,"a",@progbits
.LLSDA1993:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE1993-.LLSDACSB1993
.LLSDACSB1993:
	.uleb128 .LEHB0-.LFB1993
	.uleb128 .LEHE0-.LEHB0
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB1-.LFB1993
	.uleb128 .LEHE1-.LEHB1
	.uleb128 .L25-.LFB1993
	.uleb128 0
	.uleb128 .LEHB2-.LFB1993
	.uleb128 .LEHE2-.LEHB2
	.uleb128 .L26-.LFB1993
	.uleb128 0
	.uleb128 .LEHB3-.LFB1993
	.uleb128 .LEHE3-.LEHB3
	.uleb128 .L27-.LFB1993
	.uleb128 0
	.uleb128 .LEHB4-.LFB1993
	.uleb128 .LEHE4-.LEHB4
	.uleb128 0
	.uleb128 0
.LLSDACSE1993:
	.text
	.size	main, .-main
	.section	.text._ZNSt6vectorI7T_ThingSaIS0_EEC2Ev,"axG",@progbits,_ZNSt6vectorI7T_ThingSaIS0_EEC5Ev,comdat
	.align 2
	.weak	_ZNSt6vectorI7T_ThingSaIS0_EEC2Ev
	.type	_ZNSt6vectorI7T_ThingSaIS0_EEC2Ev, @function
_ZNSt6vectorI7T_ThingSaIS0_EEC2Ev:
.LFB2074:
	.cfi_startproc
	.cfi_personality 0x3,__gxx_personality_v0
	.cfi_lsda 0x3,.LLSDA2074
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSt12_Vector_baseI7T_ThingSaIS0_EEC2Ev
	nop
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2074:
	.section	.gcc_except_table
.LLSDA2074:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE2074-.LLSDACSB2074
.LLSDACSB2074:
.LLSDACSE2074:
	.section	.text._ZNSt6vectorI7T_ThingSaIS0_EEC2Ev,"axG",@progbits,_ZNSt6vectorI7T_ThingSaIS0_EEC5Ev,comdat
	.size	_ZNSt6vectorI7T_ThingSaIS0_EEC2Ev, .-_ZNSt6vectorI7T_ThingSaIS0_EEC2Ev
	.weak	_ZNSt6vectorI7T_ThingSaIS0_EEC1Ev
	.set	_ZNSt6vectorI7T_ThingSaIS0_EEC1Ev,_ZNSt6vectorI7T_ThingSaIS0_EEC2Ev
	.section	.text._ZSt4moveIR7T_ThingEONSt16remove_referenceIT_E4typeEOS3_,"axG",@progbits,_ZSt4moveIR7T_ThingEONSt16remove_referenceIT_E4typeEOS3_,comdat
	.weak	_ZSt4moveIR7T_ThingEONSt16remove_referenceIT_E4typeEOS3_
	.type	_ZSt4moveIR7T_ThingEONSt16remove_referenceIT_E4typeEOS3_, @function
_ZSt4moveIR7T_ThingEONSt16remove_referenceIT_E4typeEOS3_:
.LFB2079:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2079:
	.size	_ZSt4moveIR7T_ThingEONSt16remove_referenceIT_E4typeEOS3_, .-_ZSt4moveIR7T_ThingEONSt16remove_referenceIT_E4typeEOS3_
	.section	.text._ZNSt6vectorI7T_ThingSaIS0_EE9push_backEOS0_,"axG",@progbits,_ZNSt6vectorI7T_ThingSaIS0_EE9push_backEOS0_,comdat
	.align 2
	.weak	_ZNSt6vectorI7T_ThingSaIS0_EE9push_backEOS0_
	.type	_ZNSt6vectorI7T_ThingSaIS0_EE9push_backEOS0_, @function
_ZNSt6vectorI7T_ThingSaIS0_EE9push_backEOS0_:
.LFB2078:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	-16(%rbp), %rax
	movq	%rax, %rdi
	call	_ZSt4moveIR7T_ThingEONSt16remove_referenceIT_E4typeEOS3_
	movq	%rax, %rdx
	movq	-8(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	_ZNSt6vectorI7T_ThingSaIS0_EE12emplace_backIJS0_EEEvDpOT_
	nop
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2078:
	.size	_ZNSt6vectorI7T_ThingSaIS0_EE9push_backEOS0_, .-_ZNSt6vectorI7T_ThingSaIS0_EE9push_backEOS0_
	.section	.text._ZNSt12_Vector_baseI7T_ThingSaIS0_EE12_Vector_implD2Ev,"axG",@progbits,_ZNSt12_Vector_baseI7T_ThingSaIS0_EE12_Vector_implD5Ev,comdat
	.align 2
	.weak	_ZNSt12_Vector_baseI7T_ThingSaIS0_EE12_Vector_implD2Ev
	.type	_ZNSt12_Vector_baseI7T_ThingSaIS0_EE12_Vector_implD2Ev, @function
_ZNSt12_Vector_baseI7T_ThingSaIS0_EE12_Vector_implD2Ev:
.LFB2157:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSaI7T_ThingED2Ev
	nop
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2157:
	.size	_ZNSt12_Vector_baseI7T_ThingSaIS0_EE12_Vector_implD2Ev, .-_ZNSt12_Vector_baseI7T_ThingSaIS0_EE12_Vector_implD2Ev
	.weak	_ZNSt12_Vector_baseI7T_ThingSaIS0_EE12_Vector_implD1Ev
	.set	_ZNSt12_Vector_baseI7T_ThingSaIS0_EE12_Vector_implD1Ev,_ZNSt12_Vector_baseI7T_ThingSaIS0_EE12_Vector_implD2Ev
	.section	.text._ZNSt12_Vector_baseI7T_ThingSaIS0_EEC2Ev,"axG",@progbits,_ZNSt12_Vector_baseI7T_ThingSaIS0_EEC5Ev,comdat
	.align 2
	.weak	_ZNSt12_Vector_baseI7T_ThingSaIS0_EEC2Ev
	.type	_ZNSt12_Vector_baseI7T_ThingSaIS0_EEC2Ev, @function
_ZNSt12_Vector_baseI7T_ThingSaIS0_EEC2Ev:
.LFB2159:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSt12_Vector_baseI7T_ThingSaIS0_EE12_Vector_implC1Ev
	nop
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2159:
	.size	_ZNSt12_Vector_baseI7T_ThingSaIS0_EEC2Ev, .-_ZNSt12_Vector_baseI7T_ThingSaIS0_EEC2Ev
	.weak	_ZNSt12_Vector_baseI7T_ThingSaIS0_EEC1Ev
	.set	_ZNSt12_Vector_baseI7T_ThingSaIS0_EEC1Ev,_ZNSt12_Vector_baseI7T_ThingSaIS0_EEC2Ev
	.section	.text._ZNSt12_Vector_baseI7T_ThingSaIS0_EED2Ev,"axG",@progbits,_ZNSt12_Vector_baseI7T_ThingSaIS0_EED5Ev,comdat
	.align 2
	.weak	_ZNSt12_Vector_baseI7T_ThingSaIS0_EED2Ev
	.type	_ZNSt12_Vector_baseI7T_ThingSaIS0_EED2Ev, @function
_ZNSt12_Vector_baseI7T_ThingSaIS0_EED2Ev:
.LFB2162:
	.cfi_startproc
	.cfi_personality 0x3,__gxx_personality_v0
	.cfi_lsda 0x3,.LLSDA2162
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rax
	movq	16(%rax), %rax
	movq	%rax, %rdx
	movq	-8(%rbp), %rax
	movq	(%rax), %rax
	subq	%rax, %rdx
	movq	%rdx, %rax
	sarq	$2, %rax
	movq	%rax, %rdx
	movabsq	$-3689348814741910323, %rax
	imulq	%rdx, %rax
	movq	%rax, %rdx
	movq	-8(%rbp), %rax
	movq	(%rax), %rcx
	movq	-8(%rbp), %rax
	movq	%rcx, %rsi
	movq	%rax, %rdi
	call	_ZNSt12_Vector_baseI7T_ThingSaIS0_EE13_M_deallocateEPS0_m
	movq	-8(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSt12_Vector_baseI7T_ThingSaIS0_EE12_Vector_implD1Ev
	nop
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2162:
	.section	.gcc_except_table
.LLSDA2162:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE2162-.LLSDACSB2162
.LLSDACSB2162:
.LLSDACSE2162:
	.section	.text._ZNSt12_Vector_baseI7T_ThingSaIS0_EED2Ev,"axG",@progbits,_ZNSt12_Vector_baseI7T_ThingSaIS0_EED5Ev,comdat
	.size	_ZNSt12_Vector_baseI7T_ThingSaIS0_EED2Ev, .-_ZNSt12_Vector_baseI7T_ThingSaIS0_EED2Ev
	.weak	_ZNSt12_Vector_baseI7T_ThingSaIS0_EED1Ev
	.set	_ZNSt12_Vector_baseI7T_ThingSaIS0_EED1Ev,_ZNSt12_Vector_baseI7T_ThingSaIS0_EED2Ev
	.section	.text._ZSt7forwardI7T_ThingEOT_RNSt16remove_referenceIS1_E4typeE,"axG",@progbits,_ZSt7forwardI7T_ThingEOT_RNSt16remove_referenceIS1_E4typeE,comdat
	.weak	_ZSt7forwardI7T_ThingEOT_RNSt16remove_referenceIS1_E4typeE
	.type	_ZSt7forwardI7T_ThingEOT_RNSt16remove_referenceIS1_E4typeE, @function
_ZSt7forwardI7T_ThingEOT_RNSt16remove_referenceIS1_E4typeE:
.LFB2166:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2166:
	.size	_ZSt7forwardI7T_ThingEOT_RNSt16remove_referenceIS1_E4typeE, .-_ZSt7forwardI7T_ThingEOT_RNSt16remove_referenceIS1_E4typeE
	.section	.text._ZNSt6vectorI7T_ThingSaIS0_EE12emplace_backIJS0_EEEvDpOT_,"axG",@progbits,_ZNSt6vectorI7T_ThingSaIS0_EE12emplace_backIJS0_EEEvDpOT_,comdat
	.align 2
	.weak	_ZNSt6vectorI7T_ThingSaIS0_EE12emplace_backIJS0_EEEvDpOT_
	.type	_ZNSt6vectorI7T_ThingSaIS0_EE12emplace_backIJS0_EEEvDpOT_, @function
_ZNSt6vectorI7T_ThingSaIS0_EE12emplace_backIJS0_EEEvDpOT_:
.LFB2165:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	-8(%rbp), %rax
	movq	8(%rax), %rdx
	movq	-8(%rbp), %rax
	movq	16(%rax), %rax
	cmpq	%rax, %rdx
	je	.L39
	movq	-16(%rbp), %rax
	movq	%rax, %rdi
	call	_ZSt7forwardI7T_ThingEOT_RNSt16remove_referenceIS1_E4typeE
	movq	%rax, %rdx
	movq	-8(%rbp), %rax
	movq	8(%rax), %rcx
	movq	-8(%rbp), %rax
	movq	%rcx, %rsi
	movq	%rax, %rdi
	call	_ZNSt16allocator_traitsISaI7T_ThingEE9constructIS0_JS0_EEEvRS1_PT_DpOT0_
	movq	-8(%rbp), %rax
	movq	8(%rax), %rax
	leaq	20(%rax), %rdx
	movq	-8(%rbp), %rax
	movq	%rdx, 8(%rax)
	jmp	.L41
.L39:
	movq	-16(%rbp), %rax
	movq	%rax, %rdi
	call	_ZSt7forwardI7T_ThingEOT_RNSt16remove_referenceIS1_E4typeE
	movq	%rax, %rdx
	movq	-8(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	_ZNSt6vectorI7T_ThingSaIS0_EE19_M_emplace_back_auxIJS0_EEEvDpOT_
.L41:
	nop
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2165:
	.size	_ZNSt6vectorI7T_ThingSaIS0_EE12emplace_backIJS0_EEEvDpOT_, .-_ZNSt6vectorI7T_ThingSaIS0_EE12emplace_backIJS0_EEEvDpOT_
	.weak	_ZNSt6vectorI7T_ThingSaIS0_EE12emplace_backIIS0_EEEvDpOT_
	.set	_ZNSt6vectorI7T_ThingSaIS0_EE12emplace_backIIS0_EEEvDpOT_,_ZNSt6vectorI7T_ThingSaIS0_EE12emplace_backIJS0_EEEvDpOT_
	.section	.text._ZNSt12_Vector_baseI7T_ThingSaIS0_EE12_Vector_implC2Ev,"axG",@progbits,_ZNSt12_Vector_baseI7T_ThingSaIS0_EE12_Vector_implC5Ev,comdat
	.align 2
	.weak	_ZNSt12_Vector_baseI7T_ThingSaIS0_EE12_Vector_implC2Ev
	.type	_ZNSt12_Vector_baseI7T_ThingSaIS0_EE12_Vector_implC2Ev, @function
_ZNSt12_Vector_baseI7T_ThingSaIS0_EE12_Vector_implC2Ev:
.LFB2227:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSaI7T_ThingEC2Ev
	movq	-8(%rbp), %rax
	movq	$0, (%rax)
	movq	-8(%rbp), %rax
	movq	$0, 8(%rax)
	movq	-8(%rbp), %rax
	movq	$0, 16(%rax)
	nop
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2227:
	.size	_ZNSt12_Vector_baseI7T_ThingSaIS0_EE12_Vector_implC2Ev, .-_ZNSt12_Vector_baseI7T_ThingSaIS0_EE12_Vector_implC2Ev
	.weak	_ZNSt12_Vector_baseI7T_ThingSaIS0_EE12_Vector_implC1Ev
	.set	_ZNSt12_Vector_baseI7T_ThingSaIS0_EE12_Vector_implC1Ev,_ZNSt12_Vector_baseI7T_ThingSaIS0_EE12_Vector_implC2Ev
	.section	.text._ZNSaI7T_ThingED2Ev,"axG",@progbits,_ZNSaI7T_ThingED5Ev,comdat
	.align 2
	.weak	_ZNSaI7T_ThingED2Ev
	.type	_ZNSaI7T_ThingED2Ev, @function
_ZNSaI7T_ThingED2Ev:
.LFB2230:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rax
	movq	%rax, %rdi
	call	_ZN9__gnu_cxx13new_allocatorI7T_ThingED2Ev
	nop
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2230:
	.size	_ZNSaI7T_ThingED2Ev, .-_ZNSaI7T_ThingED2Ev
	.weak	_ZNSaI7T_ThingED1Ev
	.set	_ZNSaI7T_ThingED1Ev,_ZNSaI7T_ThingED2Ev
	.section	.text._ZNSt12_Vector_baseI7T_ThingSaIS0_EE13_M_deallocateEPS0_m,"axG",@progbits,_ZNSt12_Vector_baseI7T_ThingSaIS0_EE13_M_deallocateEPS0_m,comdat
	.align 2
	.weak	_ZNSt12_Vector_baseI7T_ThingSaIS0_EE13_M_deallocateEPS0_m
	.type	_ZNSt12_Vector_baseI7T_ThingSaIS0_EE13_M_deallocateEPS0_m, @function
_ZNSt12_Vector_baseI7T_ThingSaIS0_EE13_M_deallocateEPS0_m:
.LFB2232:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	%rdx, -24(%rbp)
	cmpq	$0, -16(%rbp)
	je	.L46
	movq	-8(%rbp), %rax
	movq	-24(%rbp), %rdx
	movq	-16(%rbp), %rcx
	movq	%rcx, %rsi
	movq	%rax, %rdi
	call	_ZNSt16allocator_traitsISaI7T_ThingEE10deallocateERS1_PS0_m
.L46:
	nop
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2232:
	.size	_ZNSt12_Vector_baseI7T_ThingSaIS0_EE13_M_deallocateEPS0_m, .-_ZNSt12_Vector_baseI7T_ThingSaIS0_EE13_M_deallocateEPS0_m
	.section	.text._ZNSt16allocator_traitsISaI7T_ThingEE9constructIS0_JS0_EEEvRS1_PT_DpOT0_,"axG",@progbits,_ZNSt16allocator_traitsISaI7T_ThingEE9constructIS0_JS0_EEEvRS1_PT_DpOT0_,comdat
	.weak	_ZNSt16allocator_traitsISaI7T_ThingEE9constructIS0_JS0_EEEvRS1_PT_DpOT0_
	.type	_ZNSt16allocator_traitsISaI7T_ThingEE9constructIS0_JS0_EEEvRS1_PT_DpOT0_, @function
_ZNSt16allocator_traitsISaI7T_ThingEE9constructIS0_JS0_EEEvRS1_PT_DpOT0_:
.LFB2233:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	%rdx, -24(%rbp)
	movq	-24(%rbp), %rax
	movq	%rax, %rdi
	call	_ZSt7forwardI7T_ThingEOT_RNSt16remove_referenceIS1_E4typeE
	movq	%rax, %rdx
	movq	-16(%rbp), %rcx
	movq	-8(%rbp), %rax
	movq	%rcx, %rsi
	movq	%rax, %rdi
	call	_ZN9__gnu_cxx13new_allocatorI7T_ThingE9constructIS1_JS1_EEEvPT_DpOT0_
	nop
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2233:
	.size	_ZNSt16allocator_traitsISaI7T_ThingEE9constructIS0_JS0_EEEvRS1_PT_DpOT0_, .-_ZNSt16allocator_traitsISaI7T_ThingEE9constructIS0_JS0_EEEvRS1_PT_DpOT0_
	.weak	_ZNSt16allocator_traitsISaI7T_ThingEE9constructIS0_IS0_EEEvRS1_PT_DpOT0_
	.set	_ZNSt16allocator_traitsISaI7T_ThingEE9constructIS0_IS0_EEEvRS1_PT_DpOT0_,_ZNSt16allocator_traitsISaI7T_ThingEE9constructIS0_JS0_EEEvRS1_PT_DpOT0_
	.section	.rodata
.LC2:
	.string	"vector::_M_emplace_back_aux"
	.section	.text._ZNSt6vectorI7T_ThingSaIS0_EE19_M_emplace_back_auxIJS0_EEEvDpOT_,"axG",@progbits,_ZNSt6vectorI7T_ThingSaIS0_EE19_M_emplace_back_auxIJS0_EEEvDpOT_,comdat
	.align 2
	.weak	_ZNSt6vectorI7T_ThingSaIS0_EE19_M_emplace_back_auxIJS0_EEEvDpOT_
	.type	_ZNSt6vectorI7T_ThingSaIS0_EE19_M_emplace_back_auxIJS0_EEEvDpOT_, @function
_ZNSt6vectorI7T_ThingSaIS0_EE19_M_emplace_back_auxIJS0_EEEvDpOT_:
.LFB2234:
	.cfi_startproc
	.cfi_personality 0x3,__gxx_personality_v0
	.cfi_lsda 0x3,.LLSDA2234
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -24
	movq	%rdi, -56(%rbp)
	movq	%rsi, -64(%rbp)
	movq	-56(%rbp), %rax
	movl	$.LC2, %edx
	movl	$1, %esi
	movq	%rax, %rdi
.LEHB5:
	call	_ZNKSt6vectorI7T_ThingSaIS0_EE12_M_check_lenEmPKc
	movq	%rax, -32(%rbp)
	movq	-56(%rbp), %rax
	movq	-32(%rbp), %rdx
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	_ZNSt12_Vector_baseI7T_ThingSaIS0_EE11_M_allocateEm
.LEHE5:
	movq	%rax, -24(%rbp)
	movq	-24(%rbp), %rax
	movq	%rax, -40(%rbp)
	movq	-64(%rbp), %rax
	movq	%rax, %rdi
	call	_ZSt7forwardI7T_ThingEOT_RNSt16remove_referenceIS1_E4typeE
	movq	%rax, %rbx
	movq	-56(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNKSt6vectorI7T_ThingSaIS0_EE4sizeEv
	movq	%rax, %rdx
	movq	%rdx, %rax
	salq	$2, %rax
	addq	%rdx, %rax
	salq	$2, %rax
	movq	%rax, %rdx
	movq	-24(%rbp), %rax
	leaq	(%rdx,%rax), %rcx
	movq	-56(%rbp), %rax
	movq	%rbx, %rdx
	movq	%rcx, %rsi
	movq	%rax, %rdi
	call	_ZNSt16allocator_traitsISaI7T_ThingEE9constructIS0_JS0_EEEvRS1_PT_DpOT0_
	movq	$0, -40(%rbp)
	movq	-56(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSt12_Vector_baseI7T_ThingSaIS0_EE19_M_get_Tp_allocatorEv
	movq	%rax, %rcx
	movq	-56(%rbp), %rax
	movq	8(%rax), %rsi
	movq	-56(%rbp), %rax
	movq	(%rax), %rax
	movq	-24(%rbp), %rdx
	movq	%rax, %rdi
.LEHB6:
	call	_ZSt34__uninitialized_move_if_noexcept_aIP7T_ThingS1_SaIS0_EET0_T_S4_S3_RT1_
.LEHE6:
	movq	%rax, -40(%rbp)
	addq	$20, -40(%rbp)
	movq	-56(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSt12_Vector_baseI7T_ThingSaIS0_EE19_M_get_Tp_allocatorEv
	movq	%rax, %rdx
	movq	-56(%rbp), %rax
	movq	8(%rax), %rcx
	movq	-56(%rbp), %rax
	movq	(%rax), %rax
	movq	%rcx, %rsi
	movq	%rax, %rdi
.LEHB7:
	call	_ZSt8_DestroyIP7T_ThingS0_EvT_S2_RSaIT0_E
	movq	-56(%rbp), %rax
	movq	16(%rax), %rax
	movq	%rax, %rdx
	movq	-56(%rbp), %rax
	movq	(%rax), %rax
	subq	%rax, %rdx
	movq	%rdx, %rax
	sarq	$2, %rax
	movq	%rax, %rdx
	movabsq	$-3689348814741910323, %rax
	imulq	%rdx, %rax
	movq	%rax, %rdx
	movq	-56(%rbp), %rax
	movq	(%rax), %rcx
	movq	-56(%rbp), %rax
	movq	%rcx, %rsi
	movq	%rax, %rdi
	call	_ZNSt12_Vector_baseI7T_ThingSaIS0_EE13_M_deallocateEPS0_m
.LEHE7:
	movq	-56(%rbp), %rax
	movq	-24(%rbp), %rdx
	movq	%rdx, (%rax)
	movq	-56(%rbp), %rax
	movq	-40(%rbp), %rdx
	movq	%rdx, 8(%rax)
	movq	-32(%rbp), %rdx
	movq	%rdx, %rax
	salq	$2, %rax
	addq	%rdx, %rax
	salq	$2, %rax
	movq	%rax, %rdx
	movq	-24(%rbp), %rax
	addq	%rax, %rdx
	movq	-56(%rbp), %rax
	movq	%rdx, 16(%rax)
	jmp	.L55
.L53:
	movq	%rax, %rdi
	call	__cxa_begin_catch
	cmpq	$0, -40(%rbp)
	jne	.L50
	movq	-56(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNKSt6vectorI7T_ThingSaIS0_EE4sizeEv
	movq	%rax, %rdx
	movq	%rdx, %rax
	salq	$2, %rax
	addq	%rdx, %rax
	salq	$2, %rax
	movq	%rax, %rdx
	movq	-24(%rbp), %rax
	addq	%rax, %rdx
	movq	-56(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
.LEHB8:
	call	_ZNSt16allocator_traitsISaI7T_ThingEE7destroyIS0_EEvRS1_PT_
	jmp	.L51
.L50:
	movq	-56(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSt12_Vector_baseI7T_ThingSaIS0_EE19_M_get_Tp_allocatorEv
	movq	%rax, %rdx
	movq	-40(%rbp), %rcx
	movq	-24(%rbp), %rax
	movq	%rcx, %rsi
	movq	%rax, %rdi
	call	_ZSt8_DestroyIP7T_ThingS0_EvT_S2_RSaIT0_E
.L51:
	movq	-56(%rbp), %rax
	movq	-32(%rbp), %rdx
	movq	-24(%rbp), %rcx
	movq	%rcx, %rsi
	movq	%rax, %rdi
	call	_ZNSt12_Vector_baseI7T_ThingSaIS0_EE13_M_deallocateEPS0_m
	call	__cxa_rethrow
.LEHE8:
.L54:
	movq	%rax, %rbx
	call	__cxa_end_catch
	movq	%rbx, %rax
	movq	%rax, %rdi
.LEHB9:
	call	_Unwind_Resume
.LEHE9:
.L55:
	addq	$56, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2234:
	.section	.gcc_except_table
	.align 4
.LLSDA2234:
	.byte	0xff
	.byte	0x3
	.uleb128 .LLSDATT2234-.LLSDATTD2234
.LLSDATTD2234:
	.byte	0x1
	.uleb128 .LLSDACSE2234-.LLSDACSB2234
.LLSDACSB2234:
	.uleb128 .LEHB5-.LFB2234
	.uleb128 .LEHE5-.LEHB5
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB6-.LFB2234
	.uleb128 .LEHE6-.LEHB6
	.uleb128 .L53-.LFB2234
	.uleb128 0x1
	.uleb128 .LEHB7-.LFB2234
	.uleb128 .LEHE7-.LEHB7
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB8-.LFB2234
	.uleb128 .LEHE8-.LEHB8
	.uleb128 .L54-.LFB2234
	.uleb128 0
	.uleb128 .LEHB9-.LFB2234
	.uleb128 .LEHE9-.LEHB9
	.uleb128 0
	.uleb128 0
.LLSDACSE2234:
	.byte	0x1
	.byte	0
	.align 4
	.long	0

.LLSDATT2234:
	.section	.text._ZNSt6vectorI7T_ThingSaIS0_EE19_M_emplace_back_auxIJS0_EEEvDpOT_,"axG",@progbits,_ZNSt6vectorI7T_ThingSaIS0_EE19_M_emplace_back_auxIJS0_EEEvDpOT_,comdat
	.size	_ZNSt6vectorI7T_ThingSaIS0_EE19_M_emplace_back_auxIJS0_EEEvDpOT_, .-_ZNSt6vectorI7T_ThingSaIS0_EE19_M_emplace_back_auxIJS0_EEEvDpOT_
	.weak	_ZNSt6vectorI7T_ThingSaIS0_EE19_M_emplace_back_auxIIS0_EEEvDpOT_
	.set	_ZNSt6vectorI7T_ThingSaIS0_EE19_M_emplace_back_auxIIS0_EEEvDpOT_,_ZNSt6vectorI7T_ThingSaIS0_EE19_M_emplace_back_auxIJS0_EEEvDpOT_
	.section	.text._ZNSaI7T_ThingEC2Ev,"axG",@progbits,_ZNSaI7T_ThingEC5Ev,comdat
	.align 2
	.weak	_ZNSaI7T_ThingEC2Ev
	.type	_ZNSaI7T_ThingEC2Ev, @function
_ZNSaI7T_ThingEC2Ev:
.LFB2258:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rax
	movq	%rax, %rdi
	call	_ZN9__gnu_cxx13new_allocatorI7T_ThingEC2Ev
	nop
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2258:
	.size	_ZNSaI7T_ThingEC2Ev, .-_ZNSaI7T_ThingEC2Ev
	.weak	_ZNSaI7T_ThingEC1Ev
	.set	_ZNSaI7T_ThingEC1Ev,_ZNSaI7T_ThingEC2Ev
	.section	.text._ZN9__gnu_cxx13new_allocatorI7T_ThingED2Ev,"axG",@progbits,_ZN9__gnu_cxx13new_allocatorI7T_ThingED5Ev,comdat
	.align 2
	.weak	_ZN9__gnu_cxx13new_allocatorI7T_ThingED2Ev
	.type	_ZN9__gnu_cxx13new_allocatorI7T_ThingED2Ev, @function
_ZN9__gnu_cxx13new_allocatorI7T_ThingED2Ev:
.LFB2261:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	%rdi, -8(%rbp)
	nop
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2261:
	.size	_ZN9__gnu_cxx13new_allocatorI7T_ThingED2Ev, .-_ZN9__gnu_cxx13new_allocatorI7T_ThingED2Ev
	.weak	_ZN9__gnu_cxx13new_allocatorI7T_ThingED1Ev
	.set	_ZN9__gnu_cxx13new_allocatorI7T_ThingED1Ev,_ZN9__gnu_cxx13new_allocatorI7T_ThingED2Ev
	.section	.text._ZNSt16allocator_traitsISaI7T_ThingEE10deallocateERS1_PS0_m,"axG",@progbits,_ZNSt16allocator_traitsISaI7T_ThingEE10deallocateERS1_PS0_m,comdat
	.weak	_ZNSt16allocator_traitsISaI7T_ThingEE10deallocateERS1_PS0_m
	.type	_ZNSt16allocator_traitsISaI7T_ThingEE10deallocateERS1_PS0_m, @function
_ZNSt16allocator_traitsISaI7T_ThingEE10deallocateERS1_PS0_m:
.LFB2263:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	%rdx, -24(%rbp)
	movq	-24(%rbp), %rdx
	movq	-16(%rbp), %rcx
	movq	-8(%rbp), %rax
	movq	%rcx, %rsi
	movq	%rax, %rdi
	call	_ZN9__gnu_cxx13new_allocatorI7T_ThingE10deallocateEPS1_m
	nop
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2263:
	.size	_ZNSt16allocator_traitsISaI7T_ThingEE10deallocateERS1_PS0_m, .-_ZNSt16allocator_traitsISaI7T_ThingEE10deallocateERS1_PS0_m
	.section	.text._ZN9__gnu_cxx13new_allocatorI7T_ThingE9constructIS1_JS1_EEEvPT_DpOT0_,"axG",@progbits,_ZN9__gnu_cxx13new_allocatorI7T_ThingE9constructIS1_JS1_EEEvPT_DpOT0_,comdat
	.align 2
	.weak	_ZN9__gnu_cxx13new_allocatorI7T_ThingE9constructIS1_JS1_EEEvPT_DpOT0_
	.type	_ZN9__gnu_cxx13new_allocatorI7T_ThingE9constructIS1_JS1_EEEvPT_DpOT0_, @function
_ZN9__gnu_cxx13new_allocatorI7T_ThingE9constructIS1_JS1_EEEvPT_DpOT0_:
.LFB2264:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -24
	movq	%rdi, -24(%rbp)
	movq	%rsi, -32(%rbp)
	movq	%rdx, -40(%rbp)
	movq	-40(%rbp), %rax
	movq	%rax, %rdi
	call	_ZSt7forwardI7T_ThingEOT_RNSt16remove_referenceIS1_E4typeE
	movq	%rax, %rbx
	movq	-32(%rbp), %rax
	movq	%rax, %rsi
	movl	$20, %edi
	call	_ZnwmPv
	testq	%rax, %rax
	je	.L62
	movq	(%rbx), %rdx
	movq	%rdx, (%rax)
	movq	8(%rbx), %rdx
	movq	%rdx, 8(%rax)
	movl	16(%rbx), %edx
	movl	%edx, 16(%rax)
.L62:
	nop
	addq	$40, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2264:
	.size	_ZN9__gnu_cxx13new_allocatorI7T_ThingE9constructIS1_JS1_EEEvPT_DpOT0_, .-_ZN9__gnu_cxx13new_allocatorI7T_ThingE9constructIS1_JS1_EEEvPT_DpOT0_
	.weak	_ZN9__gnu_cxx13new_allocatorI7T_ThingE9constructIS1_IS1_EEEvPT_DpOT0_
	.set	_ZN9__gnu_cxx13new_allocatorI7T_ThingE9constructIS1_IS1_EEEvPT_DpOT0_,_ZN9__gnu_cxx13new_allocatorI7T_ThingE9constructIS1_JS1_EEEvPT_DpOT0_
	.section	.text._ZNKSt6vectorI7T_ThingSaIS0_EE12_M_check_lenEmPKc,"axG",@progbits,_ZNKSt6vectorI7T_ThingSaIS0_EE12_M_check_lenEmPKc,comdat
	.align 2
	.weak	_ZNKSt6vectorI7T_ThingSaIS0_EE12_M_check_lenEmPKc
	.type	_ZNKSt6vectorI7T_ThingSaIS0_EE12_M_check_lenEmPKc, @function
_ZNKSt6vectorI7T_ThingSaIS0_EE12_M_check_lenEmPKc:
.LFB2265:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -24
	movq	%rdi, -56(%rbp)
	movq	%rsi, -64(%rbp)
	movq	%rdx, -72(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	-56(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNKSt6vectorI7T_ThingSaIS0_EE8max_sizeEv
	movq	%rax, %rbx
	movq	-56(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNKSt6vectorI7T_ThingSaIS0_EE4sizeEv
	subq	%rax, %rbx
	movq	%rbx, %rdx
	movq	-64(%rbp), %rax
	cmpq	%rax, %rdx
	setb	%al
	testb	%al, %al
	je	.L64
	movq	-72(%rbp), %rax
	movq	%rax, %rdi
	call	_ZSt20__throw_length_errorPKc
.L64:
	movq	-56(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNKSt6vectorI7T_ThingSaIS0_EE4sizeEv
	movq	%rax, %rbx
	movq	-56(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNKSt6vectorI7T_ThingSaIS0_EE4sizeEv
	movq	%rax, -40(%rbp)
	leaq	-64(%rbp), %rdx
	leaq	-40(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	_ZSt3maxImERKT_S2_S2_
	movq	(%rax), %rax
	addq	%rbx, %rax
	movq	%rax, -32(%rbp)
	movq	-56(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNKSt6vectorI7T_ThingSaIS0_EE4sizeEv
	cmpq	-32(%rbp), %rax
	ja	.L65
	movq	-56(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNKSt6vectorI7T_ThingSaIS0_EE8max_sizeEv
	cmpq	-32(%rbp), %rax
	jnb	.L66
.L65:
	movq	-56(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNKSt6vectorI7T_ThingSaIS0_EE8max_sizeEv
	jmp	.L68
.L66:
	movq	-32(%rbp), %rax
.L68:
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	je	.L69
	call	__stack_chk_fail
.L69:
	addq	$72, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2265:
	.size	_ZNKSt6vectorI7T_ThingSaIS0_EE12_M_check_lenEmPKc, .-_ZNKSt6vectorI7T_ThingSaIS0_EE12_M_check_lenEmPKc
	.section	.text._ZNSt12_Vector_baseI7T_ThingSaIS0_EE11_M_allocateEm,"axG",@progbits,_ZNSt12_Vector_baseI7T_ThingSaIS0_EE11_M_allocateEm,comdat
	.align 2
	.weak	_ZNSt12_Vector_baseI7T_ThingSaIS0_EE11_M_allocateEm
	.type	_ZNSt12_Vector_baseI7T_ThingSaIS0_EE11_M_allocateEm, @function
_ZNSt12_Vector_baseI7T_ThingSaIS0_EE11_M_allocateEm:
.LFB2266:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	cmpq	$0, -16(%rbp)
	je	.L71
	movq	-8(%rbp), %rax
	movq	-16(%rbp), %rdx
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	_ZNSt16allocator_traitsISaI7T_ThingEE8allocateERS1_m
	jmp	.L73
.L71:
	movl	$0, %eax
.L73:
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2266:
	.size	_ZNSt12_Vector_baseI7T_ThingSaIS0_EE11_M_allocateEm, .-_ZNSt12_Vector_baseI7T_ThingSaIS0_EE11_M_allocateEm
	.section	.text._ZNKSt6vectorI7T_ThingSaIS0_EE4sizeEv,"axG",@progbits,_ZNKSt6vectorI7T_ThingSaIS0_EE4sizeEv,comdat
	.align 2
	.weak	_ZNKSt6vectorI7T_ThingSaIS0_EE4sizeEv
	.type	_ZNKSt6vectorI7T_ThingSaIS0_EE4sizeEv, @function
_ZNKSt6vectorI7T_ThingSaIS0_EE4sizeEv:
.LFB2267:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rax
	movq	8(%rax), %rax
	movq	%rax, %rdx
	movq	-8(%rbp), %rax
	movq	(%rax), %rax
	subq	%rax, %rdx
	movq	%rdx, %rax
	sarq	$2, %rax
	movq	%rax, %rdx
	movabsq	$-3689348814741910323, %rax
	imulq	%rdx, %rax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2267:
	.size	_ZNKSt6vectorI7T_ThingSaIS0_EE4sizeEv, .-_ZNKSt6vectorI7T_ThingSaIS0_EE4sizeEv
	.section	.text._ZNSt12_Vector_baseI7T_ThingSaIS0_EE19_M_get_Tp_allocatorEv,"axG",@progbits,_ZNSt12_Vector_baseI7T_ThingSaIS0_EE19_M_get_Tp_allocatorEv,comdat
	.align 2
	.weak	_ZNSt12_Vector_baseI7T_ThingSaIS0_EE19_M_get_Tp_allocatorEv
	.type	_ZNSt12_Vector_baseI7T_ThingSaIS0_EE19_M_get_Tp_allocatorEv, @function
_ZNSt12_Vector_baseI7T_ThingSaIS0_EE19_M_get_Tp_allocatorEv:
.LFB2268:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2268:
	.size	_ZNSt12_Vector_baseI7T_ThingSaIS0_EE19_M_get_Tp_allocatorEv, .-_ZNSt12_Vector_baseI7T_ThingSaIS0_EE19_M_get_Tp_allocatorEv
	.section	.text._ZSt34__uninitialized_move_if_noexcept_aIP7T_ThingS1_SaIS0_EET0_T_S4_S3_RT1_,"axG",@progbits,_ZSt34__uninitialized_move_if_noexcept_aIP7T_ThingS1_SaIS0_EET0_T_S4_S3_RT1_,comdat
	.weak	_ZSt34__uninitialized_move_if_noexcept_aIP7T_ThingS1_SaIS0_EET0_T_S4_S3_RT1_
	.type	_ZSt34__uninitialized_move_if_noexcept_aIP7T_ThingS1_SaIS0_EET0_T_S4_S3_RT1_, @function
_ZSt34__uninitialized_move_if_noexcept_aIP7T_ThingS1_SaIS0_EET0_T_S4_S3_RT1_:
.LFB2269:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -24
	movq	%rdi, -24(%rbp)
	movq	%rsi, -32(%rbp)
	movq	%rdx, -40(%rbp)
	movq	%rcx, -48(%rbp)
	movq	-32(%rbp), %rax
	movq	%rax, %rdi
	call	_ZSt32__make_move_if_noexcept_iteratorIP7T_ThingSt13move_iteratorIS1_EET0_T_
	movq	%rax, %rbx
	movq	-24(%rbp), %rax
	movq	%rax, %rdi
	call	_ZSt32__make_move_if_noexcept_iteratorIP7T_ThingSt13move_iteratorIS1_EET0_T_
	movq	%rax, %rdi
	movq	-48(%rbp), %rdx
	movq	-40(%rbp), %rax
	movq	%rdx, %rcx
	movq	%rax, %rdx
	movq	%rbx, %rsi
	call	_ZSt22__uninitialized_copy_aISt13move_iteratorIP7T_ThingES2_S1_ET0_T_S5_S4_RSaIT1_E
	addq	$40, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2269:
	.size	_ZSt34__uninitialized_move_if_noexcept_aIP7T_ThingS1_SaIS0_EET0_T_S4_S3_RT1_, .-_ZSt34__uninitialized_move_if_noexcept_aIP7T_ThingS1_SaIS0_EET0_T_S4_S3_RT1_
	.section	.text._ZNSt16allocator_traitsISaI7T_ThingEE7destroyIS0_EEvRS1_PT_,"axG",@progbits,_ZNSt16allocator_traitsISaI7T_ThingEE7destroyIS0_EEvRS1_PT_,comdat
	.weak	_ZNSt16allocator_traitsISaI7T_ThingEE7destroyIS0_EEvRS1_PT_
	.type	_ZNSt16allocator_traitsISaI7T_ThingEE7destroyIS0_EEvRS1_PT_, @function
_ZNSt16allocator_traitsISaI7T_ThingEE7destroyIS0_EEvRS1_PT_:
.LFB2270:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	-16(%rbp), %rdx
	movq	-8(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	_ZN9__gnu_cxx13new_allocatorI7T_ThingE7destroyIS1_EEvPT_
	nop
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2270:
	.size	_ZNSt16allocator_traitsISaI7T_ThingEE7destroyIS0_EEvRS1_PT_, .-_ZNSt16allocator_traitsISaI7T_ThingEE7destroyIS0_EEvRS1_PT_
	.section	.text._ZSt8_DestroyIP7T_ThingS0_EvT_S2_RSaIT0_E,"axG",@progbits,_ZSt8_DestroyIP7T_ThingS0_EvT_S2_RSaIT0_E,comdat
	.weak	_ZSt8_DestroyIP7T_ThingS0_EvT_S2_RSaIT0_E
	.type	_ZSt8_DestroyIP7T_ThingS0_EvT_S2_RSaIT0_E, @function
_ZSt8_DestroyIP7T_ThingS0_EvT_S2_RSaIT0_E:
.LFB2271:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	%rdx, -24(%rbp)
	movq	-16(%rbp), %rdx
	movq	-8(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	_ZSt8_DestroyIP7T_ThingEvT_S2_
	nop
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2271:
	.size	_ZSt8_DestroyIP7T_ThingS0_EvT_S2_RSaIT0_E, .-_ZSt8_DestroyIP7T_ThingS0_EvT_S2_RSaIT0_E
	.section	.text._ZN9__gnu_cxx13new_allocatorI7T_ThingEC2Ev,"axG",@progbits,_ZN9__gnu_cxx13new_allocatorI7T_ThingEC5Ev,comdat
	.align 2
	.weak	_ZN9__gnu_cxx13new_allocatorI7T_ThingEC2Ev
	.type	_ZN9__gnu_cxx13new_allocatorI7T_ThingEC2Ev, @function
_ZN9__gnu_cxx13new_allocatorI7T_ThingEC2Ev:
.LFB2282:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	%rdi, -8(%rbp)
	nop
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2282:
	.size	_ZN9__gnu_cxx13new_allocatorI7T_ThingEC2Ev, .-_ZN9__gnu_cxx13new_allocatorI7T_ThingEC2Ev
	.weak	_ZN9__gnu_cxx13new_allocatorI7T_ThingEC1Ev
	.set	_ZN9__gnu_cxx13new_allocatorI7T_ThingEC1Ev,_ZN9__gnu_cxx13new_allocatorI7T_ThingEC2Ev
	.section	.text._ZN9__gnu_cxx13new_allocatorI7T_ThingE10deallocateEPS1_m,"axG",@progbits,_ZN9__gnu_cxx13new_allocatorI7T_ThingE10deallocateEPS1_m,comdat
	.align 2
	.weak	_ZN9__gnu_cxx13new_allocatorI7T_ThingE10deallocateEPS1_m
	.type	_ZN9__gnu_cxx13new_allocatorI7T_ThingE10deallocateEPS1_m, @function
_ZN9__gnu_cxx13new_allocatorI7T_ThingE10deallocateEPS1_m:
.LFB2284:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	%rdx, -24(%rbp)
	movq	-16(%rbp), %rax
	movq	%rax, %rdi
	call	_ZdlPv
	nop
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2284:
	.size	_ZN9__gnu_cxx13new_allocatorI7T_ThingE10deallocateEPS1_m, .-_ZN9__gnu_cxx13new_allocatorI7T_ThingE10deallocateEPS1_m
	.section	.text._ZNKSt6vectorI7T_ThingSaIS0_EE8max_sizeEv,"axG",@progbits,_ZNKSt6vectorI7T_ThingSaIS0_EE8max_sizeEv,comdat
	.align 2
	.weak	_ZNKSt6vectorI7T_ThingSaIS0_EE8max_sizeEv
	.type	_ZNKSt6vectorI7T_ThingSaIS0_EE8max_sizeEv, @function
_ZNKSt6vectorI7T_ThingSaIS0_EE8max_sizeEv:
.LFB2285:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNKSt12_Vector_baseI7T_ThingSaIS0_EE19_M_get_Tp_allocatorEv
	movq	%rax, %rdi
	call	_ZNSt16allocator_traitsISaI7T_ThingEE8max_sizeERKS1_
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2285:
	.size	_ZNKSt6vectorI7T_ThingSaIS0_EE8max_sizeEv, .-_ZNKSt6vectorI7T_ThingSaIS0_EE8max_sizeEv
	.section	.text._ZSt3maxImERKT_S2_S2_,"axG",@progbits,_ZSt3maxImERKT_S2_S2_,comdat
	.weak	_ZSt3maxImERKT_S2_S2_
	.type	_ZSt3maxImERKT_S2_S2_, @function
_ZSt3maxImERKT_S2_S2_:
.LFB2286:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	-8(%rbp), %rax
	movq	(%rax), %rdx
	movq	-16(%rbp), %rax
	movq	(%rax), %rax
	cmpq	%rax, %rdx
	jnb	.L87
	movq	-16(%rbp), %rax
	jmp	.L88
.L87:
	movq	-8(%rbp), %rax
.L88:
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2286:
	.size	_ZSt3maxImERKT_S2_S2_, .-_ZSt3maxImERKT_S2_S2_
	.section	.text._ZNSt16allocator_traitsISaI7T_ThingEE8allocateERS1_m,"axG",@progbits,_ZNSt16allocator_traitsISaI7T_ThingEE8allocateERS1_m,comdat
	.weak	_ZNSt16allocator_traitsISaI7T_ThingEE8allocateERS1_m
	.type	_ZNSt16allocator_traitsISaI7T_ThingEE8allocateERS1_m, @function
_ZNSt16allocator_traitsISaI7T_ThingEE8allocateERS1_m:
.LFB2287:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	-16(%rbp), %rcx
	movq	-8(%rbp), %rax
	movl	$0, %edx
	movq	%rcx, %rsi
	movq	%rax, %rdi
	call	_ZN9__gnu_cxx13new_allocatorI7T_ThingE8allocateEmPKv
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2287:
	.size	_ZNSt16allocator_traitsISaI7T_ThingEE8allocateERS1_m, .-_ZNSt16allocator_traitsISaI7T_ThingEE8allocateERS1_m
	.section	.text._ZSt32__make_move_if_noexcept_iteratorIP7T_ThingSt13move_iteratorIS1_EET0_T_,"axG",@progbits,_ZSt32__make_move_if_noexcept_iteratorIP7T_ThingSt13move_iteratorIS1_EET0_T_,comdat
	.weak	_ZSt32__make_move_if_noexcept_iteratorIP7T_ThingSt13move_iteratorIS1_EET0_T_
	.type	_ZSt32__make_move_if_noexcept_iteratorIP7T_ThingSt13move_iteratorIS1_EET0_T_, @function
_ZSt32__make_move_if_noexcept_iteratorIP7T_ThingSt13move_iteratorIS1_EET0_T_:
.LFB2288:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movq	%rdi, -24(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movq	-24(%rbp), %rdx
	leaq	-16(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	_ZNSt13move_iteratorIP7T_ThingEC1ES1_
	movq	-16(%rbp), %rax
	movq	-8(%rbp), %rcx
	xorq	%fs:40, %rcx
	je	.L93
	call	__stack_chk_fail
.L93:
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2288:
	.size	_ZSt32__make_move_if_noexcept_iteratorIP7T_ThingSt13move_iteratorIS1_EET0_T_, .-_ZSt32__make_move_if_noexcept_iteratorIP7T_ThingSt13move_iteratorIS1_EET0_T_
	.section	.text._ZSt22__uninitialized_copy_aISt13move_iteratorIP7T_ThingES2_S1_ET0_T_S5_S4_RSaIT1_E,"axG",@progbits,_ZSt22__uninitialized_copy_aISt13move_iteratorIP7T_ThingES2_S1_ET0_T_S5_S4_RSaIT1_E,comdat
	.weak	_ZSt22__uninitialized_copy_aISt13move_iteratorIP7T_ThingES2_S1_ET0_T_S5_S4_RSaIT1_E
	.type	_ZSt22__uninitialized_copy_aISt13move_iteratorIP7T_ThingES2_S1_ET0_T_S5_S4_RSaIT1_E, @function
_ZSt22__uninitialized_copy_aISt13move_iteratorIP7T_ThingES2_S1_ET0_T_S5_S4_RSaIT1_E:
.LFB2289:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movq	%rdi, -16(%rbp)
	movq	%rsi, -32(%rbp)
	movq	%rdx, -24(%rbp)
	movq	%rcx, -8(%rbp)
	movq	-24(%rbp), %rdx
	movq	-32(%rbp), %rcx
	movq	-16(%rbp), %rax
	movq	%rcx, %rsi
	movq	%rax, %rdi
	call	_ZSt18uninitialized_copyISt13move_iteratorIP7T_ThingES2_ET0_T_S5_S4_
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2289:
	.size	_ZSt22__uninitialized_copy_aISt13move_iteratorIP7T_ThingES2_S1_ET0_T_S5_S4_RSaIT1_E, .-_ZSt22__uninitialized_copy_aISt13move_iteratorIP7T_ThingES2_S1_ET0_T_S5_S4_RSaIT1_E
	.section	.text._ZN9__gnu_cxx13new_allocatorI7T_ThingE7destroyIS1_EEvPT_,"axG",@progbits,_ZN9__gnu_cxx13new_allocatorI7T_ThingE7destroyIS1_EEvPT_,comdat
	.align 2
	.weak	_ZN9__gnu_cxx13new_allocatorI7T_ThingE7destroyIS1_EEvPT_
	.type	_ZN9__gnu_cxx13new_allocatorI7T_ThingE7destroyIS1_EEvPT_, @function
_ZN9__gnu_cxx13new_allocatorI7T_ThingE7destroyIS1_EEvPT_:
.LFB2290:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	nop
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2290:
	.size	_ZN9__gnu_cxx13new_allocatorI7T_ThingE7destroyIS1_EEvPT_, .-_ZN9__gnu_cxx13new_allocatorI7T_ThingE7destroyIS1_EEvPT_
	.section	.text._ZSt8_DestroyIP7T_ThingEvT_S2_,"axG",@progbits,_ZSt8_DestroyIP7T_ThingEvT_S2_,comdat
	.weak	_ZSt8_DestroyIP7T_ThingEvT_S2_
	.type	_ZSt8_DestroyIP7T_ThingEvT_S2_, @function
_ZSt8_DestroyIP7T_ThingEvT_S2_:
.LFB2291:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	-16(%rbp), %rdx
	movq	-8(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	_ZNSt12_Destroy_auxILb1EE9__destroyIP7T_ThingEEvT_S4_
	nop
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2291:
	.size	_ZSt8_DestroyIP7T_ThingEvT_S2_, .-_ZSt8_DestroyIP7T_ThingEvT_S2_
	.section	.text._ZNSt16allocator_traitsISaI7T_ThingEE8max_sizeERKS1_,"axG",@progbits,_ZNSt16allocator_traitsISaI7T_ThingEE8max_sizeERKS1_,comdat
	.weak	_ZNSt16allocator_traitsISaI7T_ThingEE8max_sizeERKS1_
	.type	_ZNSt16allocator_traitsISaI7T_ThingEE8max_sizeERKS1_, @function
_ZNSt16allocator_traitsISaI7T_ThingEE8max_sizeERKS1_:
.LFB2301:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNK9__gnu_cxx13new_allocatorI7T_ThingE8max_sizeEv
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2301:
	.size	_ZNSt16allocator_traitsISaI7T_ThingEE8max_sizeERKS1_, .-_ZNSt16allocator_traitsISaI7T_ThingEE8max_sizeERKS1_
	.section	.text._ZNKSt12_Vector_baseI7T_ThingSaIS0_EE19_M_get_Tp_allocatorEv,"axG",@progbits,_ZNKSt12_Vector_baseI7T_ThingSaIS0_EE19_M_get_Tp_allocatorEv,comdat
	.align 2
	.weak	_ZNKSt12_Vector_baseI7T_ThingSaIS0_EE19_M_get_Tp_allocatorEv
	.type	_ZNKSt12_Vector_baseI7T_ThingSaIS0_EE19_M_get_Tp_allocatorEv, @function
_ZNKSt12_Vector_baseI7T_ThingSaIS0_EE19_M_get_Tp_allocatorEv:
.LFB2302:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2302:
	.size	_ZNKSt12_Vector_baseI7T_ThingSaIS0_EE19_M_get_Tp_allocatorEv, .-_ZNKSt12_Vector_baseI7T_ThingSaIS0_EE19_M_get_Tp_allocatorEv
	.section	.text._ZN9__gnu_cxx13new_allocatorI7T_ThingE8allocateEmPKv,"axG",@progbits,_ZN9__gnu_cxx13new_allocatorI7T_ThingE8allocateEmPKv,comdat
	.align 2
	.weak	_ZN9__gnu_cxx13new_allocatorI7T_ThingE8allocateEmPKv
	.type	_ZN9__gnu_cxx13new_allocatorI7T_ThingE8allocateEmPKv, @function
_ZN9__gnu_cxx13new_allocatorI7T_ThingE8allocateEmPKv:
.LFB2303:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	%rdx, -24(%rbp)
	movq	-8(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNK9__gnu_cxx13new_allocatorI7T_ThingE8max_sizeEv
	cmpq	-16(%rbp), %rax
	setb	%al
	testb	%al, %al
	je	.L103
	call	_ZSt17__throw_bad_allocv
.L103:
	movq	-16(%rbp), %rdx
	movq	%rdx, %rax
	salq	$2, %rax
	addq	%rdx, %rax
	salq	$2, %rax
	movq	%rax, %rdi
	call	_Znwm
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2303:
	.size	_ZN9__gnu_cxx13new_allocatorI7T_ThingE8allocateEmPKv, .-_ZN9__gnu_cxx13new_allocatorI7T_ThingE8allocateEmPKv
	.section	.text._ZNSt13move_iteratorIP7T_ThingEC2ES1_,"axG",@progbits,_ZNSt13move_iteratorIP7T_ThingEC5ES1_,comdat
	.align 2
	.weak	_ZNSt13move_iteratorIP7T_ThingEC2ES1_
	.type	_ZNSt13move_iteratorIP7T_ThingEC2ES1_, @function
_ZNSt13move_iteratorIP7T_ThingEC2ES1_:
.LFB2305:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	-8(%rbp), %rax
	movq	-16(%rbp), %rdx
	movq	%rdx, (%rax)
	nop
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2305:
	.size	_ZNSt13move_iteratorIP7T_ThingEC2ES1_, .-_ZNSt13move_iteratorIP7T_ThingEC2ES1_
	.weak	_ZNSt13move_iteratorIP7T_ThingEC1ES1_
	.set	_ZNSt13move_iteratorIP7T_ThingEC1ES1_,_ZNSt13move_iteratorIP7T_ThingEC2ES1_
	.section	.text._ZSt18uninitialized_copyISt13move_iteratorIP7T_ThingES2_ET0_T_S5_S4_,"axG",@progbits,_ZSt18uninitialized_copyISt13move_iteratorIP7T_ThingES2_ET0_T_S5_S4_,comdat
	.weak	_ZSt18uninitialized_copyISt13move_iteratorIP7T_ThingES2_ET0_T_S5_S4_
	.type	_ZSt18uninitialized_copyISt13move_iteratorIP7T_ThingES2_ET0_T_S5_S4_, @function
_ZSt18uninitialized_copyISt13move_iteratorIP7T_ThingES2_ET0_T_S5_S4_:
.LFB2307:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$48, %rsp
	movq	%rdi, -32(%rbp)
	movq	%rsi, -48(%rbp)
	movq	%rdx, -40(%rbp)
	movb	$1, -1(%rbp)
	movq	-40(%rbp), %rdx
	movq	-48(%rbp), %rcx
	movq	-32(%rbp), %rax
	movq	%rcx, %rsi
	movq	%rax, %rdi
	call	_ZNSt20__uninitialized_copyILb0EE13__uninit_copyISt13move_iteratorIP7T_ThingES4_EET0_T_S7_S6_
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2307:
	.size	_ZSt18uninitialized_copyISt13move_iteratorIP7T_ThingES2_ET0_T_S5_S4_, .-_ZSt18uninitialized_copyISt13move_iteratorIP7T_ThingES2_ET0_T_S5_S4_
	.section	.text._ZNSt12_Destroy_auxILb1EE9__destroyIP7T_ThingEEvT_S4_,"axG",@progbits,_ZNSt12_Destroy_auxILb1EE9__destroyIP7T_ThingEEvT_S4_,comdat
	.weak	_ZNSt12_Destroy_auxILb1EE9__destroyIP7T_ThingEEvT_S4_
	.type	_ZNSt12_Destroy_auxILb1EE9__destroyIP7T_ThingEEvT_S4_, @function
_ZNSt12_Destroy_auxILb1EE9__destroyIP7T_ThingEEvT_S4_:
.LFB2308:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	nop
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2308:
	.size	_ZNSt12_Destroy_auxILb1EE9__destroyIP7T_ThingEEvT_S4_, .-_ZNSt12_Destroy_auxILb1EE9__destroyIP7T_ThingEEvT_S4_
	.section	.text._ZNK9__gnu_cxx13new_allocatorI7T_ThingE8max_sizeEv,"axG",@progbits,_ZNK9__gnu_cxx13new_allocatorI7T_ThingE8max_sizeEv,comdat
	.align 2
	.weak	_ZNK9__gnu_cxx13new_allocatorI7T_ThingE8max_sizeEv
	.type	_ZNK9__gnu_cxx13new_allocatorI7T_ThingE8max_sizeEv, @function
_ZNK9__gnu_cxx13new_allocatorI7T_ThingE8max_sizeEv:
.LFB2315:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	%rdi, -8(%rbp)
	movabsq	$922337203685477580, %rax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2315:
	.size	_ZNK9__gnu_cxx13new_allocatorI7T_ThingE8max_sizeEv, .-_ZNK9__gnu_cxx13new_allocatorI7T_ThingE8max_sizeEv
	.section	.text._ZNSt20__uninitialized_copyILb0EE13__uninit_copyISt13move_iteratorIP7T_ThingES4_EET0_T_S7_S6_,"axG",@progbits,_ZNSt20__uninitialized_copyILb0EE13__uninit_copyISt13move_iteratorIP7T_ThingES4_EET0_T_S7_S6_,comdat
	.weak	_ZNSt20__uninitialized_copyILb0EE13__uninit_copyISt13move_iteratorIP7T_ThingES4_EET0_T_S7_S6_
	.type	_ZNSt20__uninitialized_copyILb0EE13__uninit_copyISt13move_iteratorIP7T_ThingES4_EET0_T_S7_S6_, @function
_ZNSt20__uninitialized_copyILb0EE13__uninit_copyISt13move_iteratorIP7T_ThingES4_EET0_T_S7_S6_:
.LFB2316:
	.cfi_startproc
	.cfi_personality 0x3,__gxx_personality_v0
	.cfi_lsda 0x3,.LLSDA2316
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -24
	movq	%rdi, -48(%rbp)
	movq	%rsi, -64(%rbp)
	movq	%rdx, -56(%rbp)
	movq	-56(%rbp), %rax
	movq	%rax, -24(%rbp)
.L113:
	leaq	-64(%rbp), %rdx
	leaq	-48(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
.LEHB10:
	call	_ZStneIP7T_ThingEbRKSt13move_iteratorIT_ES6_
.LEHE10:
	testb	%al, %al
	je	.L112
	leaq	-48(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNKSt13move_iteratorIP7T_ThingEdeEv
	movq	%rax, %rbx
	movq	-24(%rbp), %rax
	movq	%rax, %rdi
	call	_ZSt11__addressofI7T_ThingEPT_RS1_
	movq	%rbx, %rsi
	movq	%rax, %rdi
	call	_ZSt10_ConstructI7T_ThingJS0_EEvPT_DpOT0_
	leaq	-48(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSt13move_iteratorIP7T_ThingEppEv
	addq	$20, -24(%rbp)
	jmp	.L113
.L112:
	movq	-24(%rbp), %rax
	jmp	.L119
.L117:
	movq	%rax, %rdi
	call	__cxa_begin_catch
	movq	-24(%rbp), %rdx
	movq	-56(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	_ZSt8_DestroyIP7T_ThingEvT_S2_
.LEHB11:
	call	__cxa_rethrow
.LEHE11:
.L118:
	movq	%rax, %rbx
	call	__cxa_end_catch
	movq	%rbx, %rax
	movq	%rax, %rdi
.LEHB12:
	call	_Unwind_Resume
.LEHE12:
.L119:
	addq	$56, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2316:
	.section	.gcc_except_table
	.align 4
.LLSDA2316:
	.byte	0xff
	.byte	0x3
	.uleb128 .LLSDATT2316-.LLSDATTD2316
.LLSDATTD2316:
	.byte	0x1
	.uleb128 .LLSDACSE2316-.LLSDACSB2316
.LLSDACSB2316:
	.uleb128 .LEHB10-.LFB2316
	.uleb128 .LEHE10-.LEHB10
	.uleb128 .L117-.LFB2316
	.uleb128 0x1
	.uleb128 .LEHB11-.LFB2316
	.uleb128 .LEHE11-.LEHB11
	.uleb128 .L118-.LFB2316
	.uleb128 0
	.uleb128 .LEHB12-.LFB2316
	.uleb128 .LEHE12-.LEHB12
	.uleb128 0
	.uleb128 0
.LLSDACSE2316:
	.byte	0x1
	.byte	0
	.align 4
	.long	0

.LLSDATT2316:
	.section	.text._ZNSt20__uninitialized_copyILb0EE13__uninit_copyISt13move_iteratorIP7T_ThingES4_EET0_T_S7_S6_,"axG",@progbits,_ZNSt20__uninitialized_copyILb0EE13__uninit_copyISt13move_iteratorIP7T_ThingES4_EET0_T_S7_S6_,comdat
	.size	_ZNSt20__uninitialized_copyILb0EE13__uninit_copyISt13move_iteratorIP7T_ThingES4_EET0_T_S7_S6_, .-_ZNSt20__uninitialized_copyILb0EE13__uninit_copyISt13move_iteratorIP7T_ThingES4_EET0_T_S7_S6_
	.section	.text._ZStneIP7T_ThingEbRKSt13move_iteratorIT_ES6_,"axG",@progbits,_ZStneIP7T_ThingEbRKSt13move_iteratorIT_ES6_,comdat
	.weak	_ZStneIP7T_ThingEbRKSt13move_iteratorIT_ES6_
	.type	_ZStneIP7T_ThingEbRKSt13move_iteratorIT_ES6_, @function
_ZStneIP7T_ThingEbRKSt13move_iteratorIT_ES6_:
.LFB2317:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	-16(%rbp), %rdx
	movq	-8(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	_ZSteqIP7T_ThingEbRKSt13move_iteratorIT_ES6_
	xorl	$1, %eax
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2317:
	.size	_ZStneIP7T_ThingEbRKSt13move_iteratorIT_ES6_, .-_ZStneIP7T_ThingEbRKSt13move_iteratorIT_ES6_
	.section	.text._ZNSt13move_iteratorIP7T_ThingEppEv,"axG",@progbits,_ZNSt13move_iteratorIP7T_ThingEppEv,comdat
	.align 2
	.weak	_ZNSt13move_iteratorIP7T_ThingEppEv
	.type	_ZNSt13move_iteratorIP7T_ThingEppEv, @function
_ZNSt13move_iteratorIP7T_ThingEppEv:
.LFB2318:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rax
	movq	(%rax), %rax
	leaq	20(%rax), %rdx
	movq	-8(%rbp), %rax
	movq	%rdx, (%rax)
	movq	-8(%rbp), %rax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2318:
	.size	_ZNSt13move_iteratorIP7T_ThingEppEv, .-_ZNSt13move_iteratorIP7T_ThingEppEv
	.section	.text._ZSt11__addressofI7T_ThingEPT_RS1_,"axG",@progbits,_ZSt11__addressofI7T_ThingEPT_RS1_,comdat
	.weak	_ZSt11__addressofI7T_ThingEPT_RS1_
	.type	_ZSt11__addressofI7T_ThingEPT_RS1_, @function
_ZSt11__addressofI7T_ThingEPT_RS1_:
.LFB2319:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2319:
	.size	_ZSt11__addressofI7T_ThingEPT_RS1_, .-_ZSt11__addressofI7T_ThingEPT_RS1_
	.section	.text._ZNKSt13move_iteratorIP7T_ThingEdeEv,"axG",@progbits,_ZNKSt13move_iteratorIP7T_ThingEdeEv,comdat
	.align 2
	.weak	_ZNKSt13move_iteratorIP7T_ThingEdeEv
	.type	_ZNKSt13move_iteratorIP7T_ThingEdeEv, @function
_ZNKSt13move_iteratorIP7T_ThingEdeEv:
.LFB2320:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rax
	movq	(%rax), %rax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2320:
	.size	_ZNKSt13move_iteratorIP7T_ThingEdeEv, .-_ZNKSt13move_iteratorIP7T_ThingEdeEv
	.section	.text._ZSt10_ConstructI7T_ThingJS0_EEvPT_DpOT0_,"axG",@progbits,_ZSt10_ConstructI7T_ThingJS0_EEvPT_DpOT0_,comdat
	.weak	_ZSt10_ConstructI7T_ThingJS0_EEvPT_DpOT0_
	.type	_ZSt10_ConstructI7T_ThingJS0_EEvPT_DpOT0_, @function
_ZSt10_ConstructI7T_ThingJS0_EEvPT_DpOT0_:
.LFB2321:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -24
	movq	%rdi, -24(%rbp)
	movq	%rsi, -32(%rbp)
	movq	-32(%rbp), %rax
	movq	%rax, %rdi
	call	_ZSt7forwardI7T_ThingEOT_RNSt16remove_referenceIS1_E4typeE
	movq	%rax, %rbx
	movq	-24(%rbp), %rax
	movq	%rax, %rsi
	movl	$20, %edi
	call	_ZnwmPv
	testq	%rax, %rax
	je	.L131
	movq	(%rbx), %rdx
	movq	%rdx, (%rax)
	movq	8(%rbx), %rdx
	movq	%rdx, 8(%rax)
	movl	16(%rbx), %edx
	movl	%edx, 16(%rax)
.L131:
	nop
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2321:
	.size	_ZSt10_ConstructI7T_ThingJS0_EEvPT_DpOT0_, .-_ZSt10_ConstructI7T_ThingJS0_EEvPT_DpOT0_
	.weak	_ZSt10_ConstructI7T_ThingIS0_EEvPT_DpOT0_
	.set	_ZSt10_ConstructI7T_ThingIS0_EEvPT_DpOT0_,_ZSt10_ConstructI7T_ThingJS0_EEvPT_DpOT0_
	.section	.text._ZSteqIP7T_ThingEbRKSt13move_iteratorIT_ES6_,"axG",@progbits,_ZSteqIP7T_ThingEbRKSt13move_iteratorIT_ES6_,comdat
	.weak	_ZSteqIP7T_ThingEbRKSt13move_iteratorIT_ES6_
	.type	_ZSteqIP7T_ThingEbRKSt13move_iteratorIT_ES6_, @function
_ZSteqIP7T_ThingEbRKSt13move_iteratorIT_ES6_:
.LFB2322:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -24
	movq	%rdi, -24(%rbp)
	movq	%rsi, -32(%rbp)
	movq	-24(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNKSt13move_iteratorIP7T_ThingE4baseEv
	movq	%rax, %rbx
	movq	-32(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNKSt13move_iteratorIP7T_ThingE4baseEv
	cmpq	%rax, %rbx
	sete	%al
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2322:
	.size	_ZSteqIP7T_ThingEbRKSt13move_iteratorIT_ES6_, .-_ZSteqIP7T_ThingEbRKSt13move_iteratorIT_ES6_
	.section	.text._ZNKSt13move_iteratorIP7T_ThingE4baseEv,"axG",@progbits,_ZNKSt13move_iteratorIP7T_ThingE4baseEv,comdat
	.align 2
	.weak	_ZNKSt13move_iteratorIP7T_ThingE4baseEv
	.type	_ZNKSt13move_iteratorIP7T_ThingE4baseEv, @function
_ZNKSt13move_iteratorIP7T_ThingE4baseEv:
.LFB2323:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rax
	movq	(%rax), %rax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2323:
	.size	_ZNKSt13move_iteratorIP7T_ThingE4baseEv, .-_ZNKSt13move_iteratorIP7T_ThingE4baseEv
	.text
	.type	_Z41__static_initialization_and_destruction_0ii, @function
_Z41__static_initialization_and_destruction_0ii:
.LFB2324:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movl	%edi, -4(%rbp)
	movl	%esi, -8(%rbp)
	cmpl	$1, -4(%rbp)
	jne	.L138
	cmpl	$65535, -8(%rbp)
	jne	.L138
	movl	$_ZStL8__ioinit, %edi
	call	_ZNSt8ios_base4InitC1Ev
	movl	$__dso_handle, %edx
	movl	$_ZStL8__ioinit, %esi
	movl	$_ZNSt8ios_base4InitD1Ev, %edi
	call	__cxa_atexit
	movl	$G_data, %edi
	call	_ZNSt6vectorI7T_ThingSaIS0_EEC1Ev
	movl	$__dso_handle, %edx
	movl	$G_data, %esi
	movl	$_ZNSt6vectorI7T_ThingSaIS0_EED1Ev, %edi
	call	__cxa_atexit
.L138:
	nop
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2324:
	.size	_Z41__static_initialization_and_destruction_0ii, .-_Z41__static_initialization_and_destruction_0ii
	.section	.text._ZNSt6vectorI7T_ThingSaIS0_EED2Ev,"axG",@progbits,_ZNSt6vectorI7T_ThingSaIS0_EED5Ev,comdat
	.align 2
	.weak	_ZNSt6vectorI7T_ThingSaIS0_EED2Ev
	.type	_ZNSt6vectorI7T_ThingSaIS0_EED2Ev, @function
_ZNSt6vectorI7T_ThingSaIS0_EED2Ev:
.LFB2326:
	.cfi_startproc
	.cfi_personality 0x3,__gxx_personality_v0
	.cfi_lsda 0x3,.LLSDA2326
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSt12_Vector_baseI7T_ThingSaIS0_EE19_M_get_Tp_allocatorEv
	movq	%rax, %rdx
	movq	-8(%rbp), %rax
	movq	8(%rax), %rcx
	movq	-8(%rbp), %rax
	movq	(%rax), %rax
	movq	%rcx, %rsi
	movq	%rax, %rdi
	call	_ZSt8_DestroyIP7T_ThingS0_EvT_S2_RSaIT0_E
	movq	-8(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSt12_Vector_baseI7T_ThingSaIS0_EED2Ev
	nop
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2326:
	.section	.gcc_except_table
.LLSDA2326:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE2326-.LLSDACSB2326
.LLSDACSB2326:
.LLSDACSE2326:
	.section	.text._ZNSt6vectorI7T_ThingSaIS0_EED2Ev,"axG",@progbits,_ZNSt6vectorI7T_ThingSaIS0_EED5Ev,comdat
	.size	_ZNSt6vectorI7T_ThingSaIS0_EED2Ev, .-_ZNSt6vectorI7T_ThingSaIS0_EED2Ev
	.weak	_ZNSt6vectorI7T_ThingSaIS0_EED1Ev
	.set	_ZNSt6vectorI7T_ThingSaIS0_EED1Ev,_ZNSt6vectorI7T_ThingSaIS0_EED2Ev
	.text
	.type	_GLOBAL__sub_I_G_max_weight, @function
_GLOBAL__sub_I_G_max_weight:
.LFB2328:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movl	$65535, %esi
	movl	$1, %edi
	call	_Z41__static_initialization_and_destruction_0ii
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2328:
	.size	_GLOBAL__sub_I_G_max_weight, .-_GLOBAL__sub_I_G_max_weight
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I_G_max_weight
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 5.4.0-6ubuntu1~16.04.5) 5.4.0 20160609"
	.section	.note.GNU-stack,"",@progbits
