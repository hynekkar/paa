//
// Created by karel on 10/31/17.
//

#ifndef SAT_CSTATE_H
#define SAT_CSTATE_H

#include <vector>
#include <cstdint>
#include <random>
#include <limits.h>
#include "CFormula.h"


class CState
{
public:
    CState(CFormula *formula, double improve_prob, double random_walk_prob, double weighted_walk_prob, double cripple_coef);

    double get_opt(void) const;

    uint32_t get_instance_size( void ) const
    {
        return _formula -> get_clauses_size() + _formula -> get_variable_size();
    }

    uint32_t satisfied(void) const
    {
        return _satisfied;
    }

    double optimal_satisfied_criteria( void ) const
    {
        return _optimal_satisfied_criteria;
    }

    static CState get_new_state(const CState &state, const double k);

    bool operator>(const CState &) const;

    bool operator<(const CState &) const;

    bool operator==(const CState &) const;

    static double calculate_anneailing_weight(const CState & state);

    std::vector<bool> P_values;
private:
    CFormula * _formula;
    double _optimal_criteria;
    double _satisfied;
    double _optimal_satisfied_criteria;
    double _improve_prob;
    double _random_walk_prob;
    double _weighted_random_walk_prob;
    double _cripple_coef;

    static std::default_random_engine S_generator;
    static std::uniform_int_distribution<int> S_distribution;

    void flip(uint32_t index);

    uint32_t find_neighbour(bool no_improve = false);

    void recalculate_opt(const uint32_t);
    void recalculate_satisfied( void );



};



#endif //SAT_CSTATE_H
