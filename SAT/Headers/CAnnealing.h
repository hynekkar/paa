//
// Created by karel on 10/31/17.
//

#ifndef SAT_CANNEALING_H
#define SAT_CANNEALING_H

#include "CFormula.h"
#include "CState.h"

class CAnnealing
{
public:
    CAnnealing(CFormula * formula,
               double init_temp_devider,
               double frozen_temp_devider,
               double equilibrium_temp_devider,
               double coolrate,
               double improve_prob,
               double random_walk_prob,
               double weighted_random_walk_prob,
               double cripple_coef
    ):_max_state(formula, improve_prob, random_walk_prob, weighted_random_walk_prob, cripple_coef),
      _formula(formula),
      _temp(0),
      _init_temp_devider(init_temp_devider),
      _frozen_temp_devider(frozen_temp_devider),
      _equilibrium_temp_devider(equilibrium_temp_devider),
      _coolrate(coolrate),
      _improve_prob(improve_prob),
      _random_walk_prob(random_walk_prob),
      _weighted_random_walk_prob(weighted_random_walk_prob),
      _cripple_coef(cripple_coef)
    {
       _avg_weight = ceil((_formula->get_sum_weight()*1.0) / (_formula->get_variable_size()*1.0));
        init_temp();
    };
    CState find( void );
    uint32_t getIterations(void){return _iterations;};

private:
    CState _max_state;
    CFormula * _formula;
    double _temp;

    double _init_temp_devider; // 1.1
    double _frozen_temp_devider; // 0.1
    double _equilibrium_temp_devider; //20
    double _coolrate; //0.75
    double _avg_weight;
    double _cripple_coef;

    double _improve_prob; // 0.5
    double _random_walk_prob; // 0.5
    double _weighted_random_walk_prob; //0.1
    uint32_t _iterations;

    void init_temp( void );
    void cool( void );
    bool frozen( void );
};


#endif //SAT_CANNEALING_H
