//
// Created by karel on 10/31/17.
//

#ifndef SAT_CFORMULA_H
#define SAT_CFORMULA_H

#include <vector>
#include <cstdint>
#include <fstream>
#include <random>
#include <climits>

class CClause
{
public:
    CClause(uint32_t clause_size) :
            _clause_variables(clause_size, 0),
            _clause_variables_polarity(clause_size, true)
    {};

    void flip(uint32_t);

    void fill(const std::vector<uint32_t> &variables, const std::vector<bool> &polarity);

    bool is_satisfied(const std::vector<bool> &) const;

    uint32_t random_satisfy(void) const;

    uint32_t get_variable(const uint32_t) const;

    uint32_t get_polarity(const uint32_t) const;

    static std::default_random_engine S_generator;
    static std::uniform_int_distribution<int> S_distribution;

private:
    std::vector<uint32_t> _clause_variables;
    std::vector<bool> _clause_variables_polarity;
};

class CFormula
{
public:
    CFormula(uint32_t clauses_size,
             uint32_t variables_size,
             uint32_t variables_in_clause) :
            list_of_not_flip_variables(variables_size, not_used),
            _variables_weight(variables_size),
            _clauses(clauses_size, variables_in_clause),
            _variables_in_clause(variables_in_clause),
            _sum_weight(0),
            _last_used_index(0)
    {};

    uint32_t true_clauses(const std::vector<bool> &) const;

    bool satisfied(const std::vector<bool> &) const;

    bool fill_clauses(std::iostream &);

    bool fill_weights(std::iostream &ifs);

    uint32_t get_weight(const std::vector<bool> &) const;

    uint32_t get_variable_size(void) const;

    uint32_t get_clauses_size(void) const;

    uint32_t get_sum_weight(void) const;

    uint32_t get_variable_weight(const uint32_t index) const;

    uint32_t get_variable_index_to_improve(const std::vector<bool> &) const;

    uint32_t get_best_variable_index_to_improve( std::vector<bool> &values);
    uint32_t get_variable_index_to_improve_best_weight(const std::vector<bool> &values) const;

    enum variable_state
    {
        positive = 1, negative = 0, not_used = 2, both = 3
    };
    std::vector<variable_state> list_of_not_flip_variables;
private:
    std::vector<uint32_t> _variables_weight;
    std::vector<CClause> _clauses;
    uint32_t _variables_in_clause;
    uint32_t _sum_weight;
    uint32_t _last_used_index;

    static std::default_random_engine S_generator;
    static std::uniform_int_distribution<int> S_distribution;

};


#endif //SAT_CFORMULA_H
