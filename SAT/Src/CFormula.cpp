//
// Created by karel on 10/31/17.
//

#include "../Headers/CFormula.h"
#include <iostream>
#include <cmath>
#include <bitset>

using namespace std;

std::default_random_engine CClause::S_generator((unsigned long) (int) time(NULL));
std::uniform_int_distribution<int> CClause::S_distribution(0, (uint32_t) 2);

std::default_random_engine CFormula::S_generator((unsigned long) (int) time(NULL));
std::uniform_int_distribution<int> CFormula::S_distribution(0, (uint32_t) 0x0FFFFFFF);


void CClause::flip(const uint32_t index)
{
    if (index >= _clause_variables.size())
    {
        throw "Max_size_exception";
    }

    _clause_variables_polarity[index] = !_clause_variables_polarity[index];
}

bool CClause::is_satisfied(const std::vector<bool> &values) const
{
    for (uint32_t i = 0; i < _clause_variables_polarity.size(); i++)
    {
        if (values[_clause_variables[i]] == _clause_variables_polarity[i])
            return true;
    }
    return false;
}


void CClause::fill(const std::vector<uint32_t> &variables, const std::vector<bool> &polarity)
{
    if (variables.size() != polarity.size() || variables.size() != _clause_variables.size())
        throw "BAD vector size in fill";

    _clause_variables_polarity = polarity;
    _clause_variables = variables;
}

uint32_t CClause::random_satisfy(void) const
{
    return _clause_variables[S_distribution(S_generator)];
}


uint32_t CClause::get_polarity(const uint32_t index) const
{
    return _clause_variables_polarity[index];
}

uint32_t CClause::get_variable(const uint32_t index) const
{
    return _clause_variables[index];
}

//======================================================================================
uint32_t CFormula::true_clauses(const std::vector<bool> &variables) const
{
    uint32_t true_cnt = 0;
    for (auto it: _clauses)
    {
        if (it.is_satisfied(variables))
            true_cnt++;
    }
    return true_cnt;
}
//======================================================================================

bool CFormula::fill_clauses(std::iostream &ifs)
{
    int variable;
    for (uint32_t i = 0; i < _clauses.size(); i++)
    {
        std::vector<bool> polarity(_variables_in_clause);
        std::vector<uint32_t> variables(_variables_in_clause);
        for (uint32_t j = 0; j < _variables_in_clause; j++)
        {
            ifs >> variable;
            if (!ifs.good())
                return false;

            polarity[j] = (variable > 0) ? (1) : (0);
            variables[j] = (unsigned int) abs(variable) - 1;

            if (list_of_not_flip_variables[variables[j]] == not_used)
            {
                list_of_not_flip_variables[variables[j]] = (CFormula::variable_state) (int) polarity[j];
            } else if (polarity[j] != list_of_not_flip_variables[variables[j]]
                       && list_of_not_flip_variables[variables[j]] != both)
            {
                list_of_not_flip_variables[variables[j]] = both;
            }
        }
        _clauses[i].fill(variables, polarity);
        ifs >> variable;
        if(variable != 0)
            {
            cerr << "bad format not 0" << endl;
            }
    }
    return true;
}

bool CFormula::fill_weights(std::iostream &ifs)
{
    for (uint32_t i = 0; i < _variables_weight.size(); i++)
    {
        ifs >> _variables_weight[i];
        _sum_weight += _variables_weight[i];
        if (!ifs.good())
            return false;
    }

    return true;
}

//======================================================================================
uint32_t CFormula::get_weight(const std::vector<bool> &values) const
{
    uint32_t sum = 0;
    if (values.size() != _variables_weight.size())
    {
        throw "Vector size mismatch.";
    }
    for (uint32_t i = 0; i < values.size(); i++)
    {
        if (values[i])
            sum += _variables_weight[i];
    }

    return sum;
}

//======================================================================================
uint32_t CFormula::get_variable_size(void) const
{
    return (uint32_t) _variables_weight.size();
}

//======================================================================================
uint32_t CFormula::get_clauses_size(void) const
{
    return (uint32_t) _clauses.size();
}

//======================================================================================
uint32_t CFormula::get_sum_weight(void) const
{
    return _sum_weight;
}

//======================================================================================
bool CFormula::satisfied(const std::vector<bool> &values) const
{
    return (true_clauses(values) == _clauses.size());
}

uint32_t CFormula::get_variable_weight(const uint32_t index) const
{
    return _variables_weight[index];
}

//======================================================================================
uint32_t CFormula::get_variable_index_to_improve(const vector<bool> &values) const
{
    vector<uint32_t> unsatisfied;
    for (uint32_t i = 0; i < _clauses.size(); i++)
    {
        if (!_clauses[i].is_satisfied(values))
        {
            unsatisfied.push_back(i);
        }
    }
    uint32_t r = rand() % unsatisfied.size();
    return _clauses[unsatisfied[r]].random_satisfy();
}

//=====================================================================================
uint32_t CFormula::get_variable_index_to_improve_best_weight(const std::vector<bool> &values) const
{
    vector<uint32_t> unsatisfied;
    uint32_t max_weight =0;
    uint32_t max_weight_index = 0;
    uint32_t max_variable = 0;
    for (uint32_t i = 0; i < _clauses.size(); i++)
    {
        if (!_clauses[i].is_satisfied(values))
        {
            unsatisfied.push_back(i);
        }
    }
    uint32_t r = rand() % unsatisfied.size();
    for (uint32_t i = 0; i< _variables_in_clause; i++ )
    {
        uint32_t variable = _clauses[unsatisfied[r]].get_variable(i);
        uint32_t loop_weight = _variables_weight[variable];
        if(loop_weight > max_weight)
        {
            max_variable = variable;
            max_weight = loop_weight;
            max_weight_index = i;
        }
    }
    //cout << max_variable << " " << max_weight << endl;
    return max_variable;
}

//======================================================================================
uint32_t CFormula::get_best_variable_index_to_improve(vector<bool> &values)
{
    uint32_t orig_cnt = true_clauses(values);
    int32_t max = -100;
    uint32_t return_index = 0;


    for (uint32_t k = 0; k< _clauses.size(); k++)
    {
        if(!_clauses[k].is_satisfied(values))
        {
            for(uint32_t j = 0; j < _variables_in_clause; j++)
            {
                uint32_t variable_index = _clauses[k].get_variable(j);
                values[variable_index] = !values[variable_index];

                uint32_t cnt = 0;
                for (uint32_t i = 0; i < _clauses.size(); i++)
                    if (_clauses[i].is_satisfied(values))
                        cnt++;

                values[variable_index] = !values[variable_index];


                int diff = (int32_t) cnt - (int32_t) orig_cnt;

                if (diff > max && _last_used_index != variable_index)
                {
                    max = diff;
                    return_index = variable_index;
                }
            }
        }
    }
    return return_index;
}