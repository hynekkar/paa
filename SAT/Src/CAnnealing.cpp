//
// Created by karel on 10/31/17.
//

#include "../Headers/CAnnealing.h"
#include <iostream>

using namespace std;

bool CAnnealing::frozen(void)
{
  return _temp > 0.250 * _avg_weight  * _frozen_temp_devider;
}

void CAnnealing::cool(void)
{
  _temp *= _coolrate;
}

void CAnnealing::init_temp(void)
{

  _temp = _avg_weight  * 1.4427 * _init_temp_devider;
}
CState CAnnealing::find(void)
{
    _iterations=0;
    uint32_t no_new_state_cnt = 0;
    CState state = _max_state;
    double last_opt = 0;
    volatile uint32_t i =0;
    volatile double actual_opt = 0;
    state = _max_state;
    do
    {

      for (uint32_t i = 0; i < state.get_instance_size() * ceil(_temp /_equilibrium_temp_devider ); i++)
      {
        CState new_state = CState::get_new_state(state, _temp);

          if (!(new_state == state))
          {
          state = new_state;
          no_new_state_cnt = 0;

        } else
        {
          no_new_state_cnt++;
        }
       // cerr << state.get_opt() << endl;
          _iterations++;
        if (state > _max_state) {
            _max_state = state;
        }

      }
      cool();
    } while (frozen());
  return _max_state;
}


