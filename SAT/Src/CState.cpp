//
// Created by karel on 10/31/17.
//

#include "../Headers/CState.h"
#include <chrono>
#include <iostream>

using namespace std;

//std::chrono::system_clock::time_point now;
//================================================================================
std::default_random_engine CState::S_generator((unsigned long) (int) time(NULL));
std::uniform_int_distribution<int> CState::S_distribution(0, (uint32_t) 0x0FFFFFFF);

//================================================================================
CState::CState(CFormula *formula, double improve_prob, double random_walk_prob, double weighted_walk_prob, double cripple_coef) :
        P_values(formula->get_variable_size(), false),
        _formula(formula),
        _optimal_criteria(0),
        _optimal_satisfied_criteria(0),
        _improve_prob(improve_prob),
        _random_walk_prob(random_walk_prob),
        _weighted_random_walk_prob(weighted_walk_prob),
        _cripple_coef(cripple_coef)
{
    for (uint32_t i = 0; i < formula->list_of_not_flip_variables.size(); i++)
    {
        if (formula->list_of_not_flip_variables[i] == CFormula::positive)
            P_values[i] = 1;
        else if (formula->list_of_not_flip_variables[i] == CFormula::negative)
        {
            P_values[i] = 0;
        }
    }
    for (uint32_t i = 0; i < P_values.size() * 4; i++)
        find_neighbour(false);

}

//================================================================================
void CState::flip(const uint32_t index)
{
    if (index >= P_values.size())
    {
        throw "index size mismatch";
    }
    P_values[index] = !P_values[index];
    recalculate_opt(index);
    recalculate_satisfied();
    _satisfied = (_optimal_satisfied_criteria == _formula->get_clauses_size());
    return;
}

//================================================================================
inline uint32_t CState::find_neighbour(bool no_improve)
{
    uint32_t index_to_flip = 0;
    double rand = S_distribution(S_generator) / ((double) S_distribution.max());

        if (!_satisfied && rand < _improve_prob && !no_improve)
        {
            double in_rand = S_distribution(S_generator) / ((double) S_distribution.max());
            if(in_rand < _random_walk_prob) {
                index_to_flip = _formula->get_variable_index_to_improve(P_values);
            }else if(in_rand < _weighted_random_walk_prob + _random_walk_prob){
                index_to_flip = _formula->get_variable_index_to_improve_best_weight(P_values);
            }else
            {
                index_to_flip = _formula->get_best_variable_index_to_improve(P_values);
            }

        }else
        {
            index_to_flip = (uint32_t) (S_distribution(S_generator) % P_values.size());
        }
    flip(index_to_flip);
    return index_to_flip;
}

//================================================================================
inline void CState::recalculate_opt(const uint32_t index)
{
    if (P_values[index] == false)
    {
        _optimal_criteria -= _formula->get_variable_weight(index);
        return;
    }

    _optimal_criteria += _formula->get_variable_weight(index);
}

//================================================================================
inline void CState::recalculate_satisfied(void)
{
    _optimal_satisfied_criteria = _formula->true_clauses(P_values);
}

//================================================================================
double CState::calculate_anneailing_weight(const CState & state)
{
    double s = state.optimal_satisfied_criteria()/state._formula->get_clauses_size();
    return (state.satisfied())?(state.get_opt()):(state.get_opt()*s*state._cripple_coef);
}

//================================================================================
CState CState::get_new_state(const CState &state, const double k)
{
    CState new_state = state;
    new_state.find_neighbour();

    double opt_new = calculate_anneailing_weight(new_state);
    double opt_old = calculate_anneailing_weight(state);

    double delta = opt_new - opt_old;
    if (delta > 0)
        return new_state;

    double err = (S_distribution(S_generator) / (double) (int) S_distribution.max());
    if (err < exp(delta / k))
    {

            return new_state;
    }
    return state;
}

//================================================================================
double CState::get_opt(void) const

{
    return _optimal_criteria;
}
//================================================================================

bool CState::operator>(const CState &b) const
{
    if (this->_satisfied && !b._satisfied)
        return true;
    else if (!this->_satisfied && b._satisfied)
        return false;
    else if (this->_satisfied && b._satisfied)
        return (this->_optimal_criteria > b._optimal_criteria);
    else
        return (this->_optimal_satisfied_criteria > b._optimal_satisfied_criteria);
}

//================================================================================
bool CState::operator==(const CState &b) const
{
    return ((this->_satisfied == b._satisfied) && (this->_optimal_criteria == b._optimal_criteria));
}



//================================================================================
bool CState::operator<(const CState &b) const
{
    if ((*this) == b)
        return false;

    return !(this->operator>(b));
}
//================================================================================

