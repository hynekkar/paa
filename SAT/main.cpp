#include <iostream>
#include <fstream>
#include "Headers/CFormula.h"
#include "Headers/CAnnealing.h"
#include "Headers/CState.h"
#include <sstream>

using namespace std;

#define INPUT_FILE 1

double avg(vector<double>d)
{
    double sum = 0;
    for (auto it : d)
        sum+=it;

    return sum/d.size();
}

double avgInt(vector<uint32_t>d)
{
  double sum = 0;
  for (auto it : d)
    sum+=it;

  return sum/d.size();
}


double max(vector<double>d)
{
    double max = 0;
    for(auto it: d)
    {
        if(max < it)
        {
            max = it;
        }
    }
    return max;
}

double min(vector<double>d)
{
    double min = UINT32_MAX;
    for(auto it: d)
    {
        if(min > it)
        {
            min = it;
        }
    }
    return min;
}

double cnt(vector<double>d)
{
    double res = 0;
    for(auto it : d)
    {
        if(it == 1)
            res++;
    }
    return res;
}

int main(int argc, char **argv)
{
    if (argc < 2)
    {
        cerr << "invalid arguments" << endl;
        return 1;
    }
    ifstream ifs;
    uint32_t variables_size, clauses_size, variables_in_clause;
   // cout << "\\hline"<< endl;
   // cout << "$P_zlep$ & $\\not\\!\\circ \\; W(Y)$ & Počet spln. formulí & $\\not\\!\\circ \\; $spln. klauzulí & $\\not\\!\\circ \\;$doba běhu [ms] & rozdíl (max - min) \\\\" << endl;
   // cout <<   "\\hline" << endl;
   // cout << "\\hline" << endl;
    //cerr << "#p & $\\avg{W(Y)}$ & Počet spln. & $\\avg{spln. klauzulí}$ & $\\avg{doba běhu [ms]} & rozdíl \\\\" << endl;


//for(double d = 0.40; d < 0.99; d+=0.05) {
    vector<double> optimal_criteria;
    vector<double> optimal_sat;
    vector<double> time;
    vector<double> satisfied;
    vector<uint32_t> iterations;
    stringstream weights;
    stringstream clauses;
    for (uint32_t i = 0; i < 1; i++) {
        ifs.open(argv[INPUT_FILE]);
        char line[1024];
        while(!ifs.eof () && ifs.good())
          {
            ifs.getline (line, 1024);
            if(line[0]=='c')
              continue;

            if(line[0] == 'w')
              {
                weights << (line+1) << endl;
                continue;
              }
            if(line[0] == 'p')
              {
                char c;
                char mwcnf[1024];
                sscanf (line, "%c %1024s %d %d", &c, mwcnf, &variables_size, &clauses_size);
                variables_in_clause = 3;
                continue;
              }

            clauses << line << endl;
          }

        CFormula formula(clauses_size, variables_size, variables_in_clause);
        if (!formula.fill_clauses(clauses) || !formula.fill_weights(weights)) {
            cerr << "bad input file" << endl;
            return 0;
        }
        ifs.close();
        /*
            CAnnealing(CFormula * formula,
                       double init_temp_devider,
                double frozen_temp_devider,
                double equilibrium_temp_devider,
                double coolrate,
                double improve_prob,
                double random_walk_prob,
                double weighted_random_walk_prob,
                double cripple_coef
        */
        CAnnealing annealing(&formula, 1.5, 0.01, 15, 0.75, 0.5, 0.4, 0, 0.7);
        time_t b = clock();

        CState result = annealing.find();
        time_t e = clock();
        optimal_criteria.push_back(result.get_opt());
        time.push_back(((e - b) / (CLOCKS_PER_SEC / 1000) * 1.0));
        iterations.push_back (annealing.getIterations ());
        satisfied.push_back(result.satisfied());
        optimal_sat.push_back(result.optimal_satisfied_criteria());

    }
    cout << argv[INPUT_FILE] << " & " << avg(optimal_criteria) << " & " << cnt(satisfied) << " & " << avg(optimal_sat) << " & " << avg(time) << " & " << avgInt(iterations)<< " & " << max(optimal_criteria) - min(optimal_criteria) << " \\\\" << endl;
    cout << "\\hline" << endl;
   // cerr << 1 << ";" << avg(optimal_criteria) << ";" << cnt(satisfied) << ";" << avg(optimal_sat) << ";" << avg(time) << ";" << max(optimal_criteria) - min(optimal_criteria)  << ";" << endl;
//}
    return 0;
}
