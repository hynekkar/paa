#include <iostream>
#include <vector>
#include <fstream>
#include <time.h>
#include <cmath>
#include <iomanip>
#include <limits.h>
#include <bitset>
#include <algorithm>
#include <sstream>
#include <string>

using namespace std;

typedef struct T_tab_item
{
    T_tab_item():before_depth(0), before_price(0),weight(0),in_use(false){};

    uint32_t before_depth;
    uint32_t before_price;
    uint32_t weight;
    bool in_use;
}T_tab_item;

typedef struct T_Thing
{
    T_Thing(uint32_t weight, uint32_t price, bool in_bag, uint32_t counter):
            m_weight(weight),
            m_price(price),
            m_in_bag_flag(in_bag),
            m_counter(counter)
    {
        m_score = m_price / (m_weight * 1.0);
    };

    uint32_t m_weight;
    uint32_t m_price;
    double m_score;
    bool m_in_bag_flag;
    uint32_t m_counter;
} T_Thing;


T_tab_item ** G_dynamic_table;



vector<T_Thing> G_data;
uint32_t G_max_weight;
uint32_t G_max_price;
uint32_t G_data_size;

vector<T_Thing> G_solution;

//====================================================================================================================
//========================CODE============================
//====================================================================================================================

//=====================================READ_INPUT=====================================================================
bool read_solved_data(int id, ifstream & in, uint32_t& optimal_price)
{
    int data_size;
    int temp_id;
    in >> temp_id;
    in >> data_size;
    in >> optimal_price;
    if(temp_id != id)
    {
        cerr << "error in parsing solved data" << endl;
    }
    char check_new_line;
    while(in.get(check_new_line) && check_new_line != '\n');

    if(!in.good())
    {
        in.ignore();
        return false;
    }

    return true;

}

bool read_instance(int& id, ifstream& in)
{
    in >> id;
    in >> G_data_size;
    in >> G_max_weight;

    char check_new_line;
    uint32_t temp_weight, temp_price, counter = 0;
    while(in.get(check_new_line) && check_new_line != '\n' && G_data.size() < G_data_size)
    {

        in >> temp_weight;
        in >> temp_price;
        G_data.push_back(T_Thing(temp_weight, temp_price, false, counter++));
    }

    if(!in.good())
    {
        in.ignore();
        return false;
    }

    return true;
}

//====================================================================================================================


uint32_t get_max_price(vector<T_Thing>& data)
{
    uint32_t max = 0;
    for(auto it: data)
        max += it.m_price;

    return max;
}

#define BITSET_SIZE 500
uint32_t G_cnt = 0;
uint64_t dynamic_fast(void)
{
	uint32_t max_Price = get_max_price(G_data);
	vector<uint32_t> dp_array(max_Price + 1, 0xffffffff);
	dp_array[0] = 0;
	vector< bitset<BITSET_SIZE> > items(max_Price + 1);

	uint32_t result_price = 0;
	
	for(auto it:G_data)
	{	
		for(int i = max_Price; i >= 0; i--)
		{
			if(dp_array[i] != 0xffffffff || i == 0)
			{
				uint32_t cell_weight = dp_array[i] + it.m_weight;
				uint32_t cell_price = i + it.m_price;
				if(cell_weight > G_max_weight )
				{ 
					continue;
				}
				if(dp_array[cell_price] > cell_weight)
				{
					dp_array[cell_price] = cell_weight;
					items[cell_price] = items[i];
					items[cell_price][it.m_counter] = 1;
				}

				//dp_array[cell_price] = min(dp_array[cell_price], cell_weight);
				if(cell_price > result_price)
					result_price = cell_price;
			}
		}

	}

	G_solution = G_data;
	G_max_price = result_price;

	for(uint32_t i = 0; i< G_solution.size(); i++)
	{
		if(items[result_price][i])
		{ 
			G_solution[i].m_in_bag_flag = 1;
		}
	}


return result_price;
}



void fptas(double eps)
{
    vector<T_Thing> Data_temp = G_data;
    uint32_t max_Price = get_max_price(G_data);
    double K = ((eps/100.0)*max_Price)/ G_data.size();
    for(uint32_t i=0; i<G_data.size(); i++)
    {
        G_data[i].m_price =  floor(G_data[i].m_price / K);
    }

    dynamic_fast();
    G_max_price=0;

    //restore max_price
    for(uint32_t i=0; i<G_solution.size(); i++)
    {
        if(G_solution[i].m_in_bag_flag)
            G_max_price+=Data_temp[i].m_price;
    }
    G_data = Data_temp;
}


vector<uint32_t> G_depth_price_lookup;

void find_max_price_depth(void)
{
    vector<uint32_t> tmp;

    for(uint32_t i = 0; i < G_data.size(); i++)
    {
        uint32_t max_price_sum = 0;
        for(uint32_t j=i; j < G_data.size(); j++)
        {
            max_price_sum += G_data[j].m_price;
        }
        tmp.push_back(max_price_sum);
    }

    G_depth_price_lookup = tmp;
}


void solve_bruteforce_branch_cut(const uint32_t actual_depth, const uint32_t weight, const uint32_t price)
{
    if(price > G_max_price)
    {
        G_solution = G_data;
        G_max_price = price;
    }

    if(actual_depth < G_data.size())
    {
        //put inside

        if(G_depth_price_lookup[actual_depth] + price < G_max_price)
        {
            return;
        }

        if(weight + G_data[actual_depth].m_weight <=G_max_weight)
        {
        	G_cnt++;
            G_data[actual_depth].m_in_bag_flag = true;
            solve_bruteforce_branch_cut(actual_depth + 1, weight + G_data[actual_depth].m_weight,
                             price + G_data[actual_depth].m_price);
        }


        G_data[actual_depth].m_in_bag_flag = false;
        solve_bruteforce_branch_cut(actual_depth+1, weight , price );

    }
}



double avg(vector<double>&nums)
{
    double sum = 0;
    for (auto it : nums)
        sum+= it;

    return sum/nums.size();
}

double max(vector<double>&nums)
{
    double max = 0;
    for(auto it : nums)
    {
        if ( max < it )
        {
            max = it;
        }
    }

    return max;
}

#define ERR 5

bool cmp (T_Thing i,T_Thing j) { return (i.m_weight > j.m_weight); }
bool cmp_score (T_Thing i,T_Thing j) { return (i.m_score > j.m_score); }
void fast_solve(void)
{
    G_solution = G_data;
    sort (G_data.begin(), G_data.end(), cmp_score);

    uint32_t acc_weight = 0;

    for(auto it: G_data)
    {
        if(acc_weight + it.m_weight > G_max_weight)
            continue;
        else
        {
            acc_weight += it.m_weight;
            G_max_price+= it.m_price;
            G_solution[it.m_counter].m_in_bag_flag = true;

        }
    }

}

int main(int argc, char * argv[])
{
    if(argc < 0)
    {
        cout << "invalid arguments" << endl;
        return 1;
    }

    int id;
    vector<T_Thing>data;
    vector<double> failures;
    vector<double> dynamic_times;
    vector<double> branch_cut_times;
    vector<double> fptas_times;
    clock_t begin = 0;
    clock_t end = 0;
    ofstream of;
    of.open("out.txt");


    for(double i = 0.1; i< 4 ; i+=0.1)
    {
        stringstream temp_str;
        string to_system;
        temp_str << "./generator/knapgen/a.out -I 100 -n 30 -N 100 -m 0.5 -W 40 -C 3000 -k "<< i <<" -d -1 > temp_input.in.txt 2>/dev/null" << endl;


        getline (temp_str, to_system);

        //cout << to_system << endl; uloha 3

        system(to_system.c_str());

        ifstream input;
        ifstream check;

        input.open("temp_input.in.txt");
        //check.open(argv[2]);


        while (read_instance(id, input))
        {
            uint32_t optimal_price = 0;

            //read_solved_data(id, check, optimal_price);

            for(uint32_t j =0; j< 1; j++)
            {
            begin = clock();
           // optimal_price = dynamic_fast();
            end = clock();
            }
            dynamic_times.push_back((end - begin) / ((CLOCKS_PER_SEC / 1000) * 1.0));

            G_max_price = 0;

            begin = clock();
            sort(G_data.begin(), G_data.end(), cmp);
            find_max_price_depth();
            solve_bruteforce_branch_cut(0, 0, 0);
            end = clock();
            branch_cut_times.push_back((end - begin) / ((CLOCKS_PER_SEC / 1000) * 1.0));

            optimal_price = G_max_price;
            G_max_price = 0;

            begin = clock();
            fast_solve();
            end = clock();
            fptas_times.push_back((end - begin) / ((CLOCKS_PER_SEC / 1000) * 1.0));
            failures.push_back(abs(optimal_price - G_max_price) / (optimal_price*1.0));

            G_max_price = 0;



          /*  of << id << " " << G_data.size() << " " << G_max_price;
            for (auto it : G_solution)
                of << " " << it.m_in_bag_flag;

            of << " " << optimal_price << endl;
            */
            
            G_max_weight = 0;
            G_max_price = 0;
            G_data = vector<T_Thing>();


        }
       // cout << std::fixed << std::setprecision(5) << i << ";" << avg(failures)  << endl;
        cout << i << ";" << avg(failures) << endl; //";" << avg(branch_cut_times) << ";" << G_cnt << endl;
        failures = vector<double>();

        G_cnt = 0;
        //cout << "\\hline" << endl;

        input.close();
        //check.close();


    }
    return 0;
}