/*
 * PAA - problem batohu - hruba sila
 * Lukas Skrivanek
 * CVUT FEL
 * skrivl1@fel.cvut.cz
 * 2006
 *
 */

#include<stdio.h>
#include<stdlib.h>
#include<time.h>


typedef struct vec {
	int vaha;
	int cena;
	char vlozen;
}VEC;

void reseni_copy(char* dst,VEC* src, int n){
	int i;
	for(i=0;i<n;i++){
		dst[i]=src[i].vlozen;
	}
}

void rekurze(VEC* veci, char* reseni, int hloubka, int maxhloubka, int vaha, int maxvaha, int cena, int* reseni_cena){
	if (cena>*reseni_cena){
		*reseni_cena=cena;
		reseni_copy(reseni, veci, maxhloubka);
	}
	if(hloubka<maxhloubka){
		// pripad 1
		if((vaha+veci[hloubka].vaha)<=maxvaha){
			veci[hloubka].vlozen=1;
			rekurze(veci, reseni, hloubka+1, maxhloubka, vaha+veci[hloubka].vaha, maxvaha, cena+veci[hloubka].cena, reseni_cena);
		}
		//pripad 0
			veci[hloubka].vlozen=0;
			rekurze(veci, reseni, hloubka+1, maxhloubka, vaha, maxvaha, cena, reseni_cena);
	}
}

int main(int argc,char **argv)
{
	FILE *in;
	FILE *out;
	int ID;
	int pocet_veci;
	int kapacita_batohu;
	char* reseni;
	int reseni_cena;
	clock_t cas_zacatek;
	clock_t cas_konec;

	VEC* veci;

	/* pomocne promenne */
	int i; /* iterator */
	int prvni=1;

	if(argc>2){
		in=fopen(argv[1],"r");
		if (in==NULL){ /* chyba pri otevirani souboru */
			fprintf(stderr,"Chyba: Vstupni soubor nelze otevrit! \n");
			return 1;
		}
		out=fopen(argv[2],"w");
		if (out==NULL){ /* chyba pri otevirani souboru */
			fprintf(stderr,"Chyba: Vystupni soubor nelze otevrit! \n");
			return 1;
		}

		while(!feof(in)){
			reseni_cena=0;
			fscanf(in, "%d %d %d", &ID, &pocet_veci, &kapacita_batohu);
			printf("Instance ID: %d\n", ID);

			if(prvni==1){
				prvni=0;
				/*
				*	V CELEM JEDNOM SOUBORU MUSI BYT STEJNY POCET VECI V KAZDEM RADKU !!!!!
				*/
				veci = (VEC *) malloc(pocet_veci*sizeof(VEC));
				if (veci==NULL){ /* chyba pri alokaci pameti */
					fprintf(stderr,"Chyba: Nelze naalokovat potrebnou pamet! \n");
					return 2;
				}

				reseni = (char *) malloc(pocet_veci*sizeof(char));
				if (reseni==NULL){ /* chyba pri alokaci pameti */
					fprintf(stderr,"Chyba: Nelze naalokovat potrebnou pamet! \n");
					return 2;
				}
			}

			for(i=0; i<pocet_veci; i++) {
				fscanf(in, "%d %d", &(veci[i].vaha), &(veci[i].cena));
				veci[i].vlozen=0;
			}
			fscanf(in, "\n");

			reseni_copy(reseni, veci, pocet_veci);

			cas_zacatek = clock();

			rekurze(veci, reseni, 0, pocet_veci, 0, kapacita_batohu, 0, &reseni_cena);

			cas_konec = clock();

			fprintf(out, "%d %d %d", ID, pocet_veci, reseni_cena);
			for(i=0; i<pocet_veci; i++){
				fprintf(out, " %d", reseni[i]);
			}
			fprintf(out, " %f",((double) (cas_konec - cas_zacatek)) / CLOCKS_PER_SEC);
			fprintf(out, "\n");
		}


		free(veci);
		free(reseni);
		fclose(in);
		fclose(out);
	}else{
		printf("36PAA, Lukas Skrivanek, problem batohu reseny pomoci hrube sily");
		printf("Pouziti:\n");
		printf("%s vstupni_soubor(ve formatu: ID n M váha cena váha cena ... ...) vystupni_soubor", argv[0]);
	}
	return 0;
}


